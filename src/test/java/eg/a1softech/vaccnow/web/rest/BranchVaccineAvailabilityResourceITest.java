package eg.a1softech.vaccnow.web.rest;

import static eg.a1softech.vaccnow.web.rest.util.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eg.a1softech.vaccnow.VaccnowApplication;
import eg.a1softech.vaccnow.domain.BranchVaccineAvailability;
import eg.a1softech.vaccnow.repository.BranchVaccineAvailabilityRepository;
import eg.a1softech.vaccnow.service.BranchVaccineAvailabilityService;
import eg.a1softech.vaccnow.service.dto.BranchVaccineAvailabilityDTO;
import eg.a1softech.vaccnow.service.mapper.BranchVaccineAvailabilityMapper;
import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BranchVaccineAvailabilityResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@SpringBootTest(classes = VaccnowApplication.class)
@AutoConfigureMockMvc
class BranchVaccineAvailabilityResourceITest {

  private static final ZonedDateTime DEFAULT_VACCINATION_DATE_TIME = ZonedDateTime.ofInstant(
    Instant.ofEpochMilli(0L),
    ZoneOffset.UTC
  );
  private static final ZonedDateTime UPDATED_VACCINATION_DATE_TIME = ZonedDateTime
    .now(ZoneId.systemDefault())
    .withNano(0);

  private static final Integer DEFAULT_DURATION = 1;
  private static final Integer UPDATED_DURATION = 2;

  private static final Integer DEFAULT_AVAILABLE_SEATS = 1;
  private static final Integer UPDATED_AVAILABLE_SEATS = 2;

  @Autowired
  private BranchVaccineAvailabilityRepository branchVaccineAvailabilityRepository;

  @Autowired
  private BranchVaccineAvailabilityMapper branchVaccineAvailabilityMapper;

  @Autowired
  private BranchVaccineAvailabilityService branchVaccineAvailabilityService;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restBranchVaccineAvailabilityMockMvc;

  private BranchVaccineAvailability branchVaccineAvailability;

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static BranchVaccineAvailability createEntity(EntityManager em) {
    BranchVaccineAvailability branchVaccineAvailability = new BranchVaccineAvailability()
      .vaccinationDateTime(DEFAULT_VACCINATION_DATE_TIME)
      .duration(DEFAULT_DURATION)
      .availableSeats(DEFAULT_AVAILABLE_SEATS);
    return branchVaccineAvailability;
  }

  /**
   * Create an updated entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static BranchVaccineAvailability createUpdatedEntity(
    EntityManager em
  ) {
    BranchVaccineAvailability branchVaccineAvailability = new BranchVaccineAvailability()
      .vaccinationDateTime(UPDATED_VACCINATION_DATE_TIME)
      .duration(UPDATED_DURATION)
      .availableSeats(UPDATED_AVAILABLE_SEATS);
    return branchVaccineAvailability;
  }

  @BeforeEach
  public void initTest() {
    branchVaccineAvailability = createEntity(em);
  }

  @Test
  @Transactional
  void createBranchVaccineAvailability() throws Exception {
    int databaseSizeBeforeCreate = branchVaccineAvailabilityRepository
      .findAll()
      .size();
    // Create the BranchVaccineAvailability
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );
    restBranchVaccineAvailabilityMockMvc
      .perform(
        post("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isCreated());

    // Validate the BranchVaccineAvailability in the database
    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList)
      .hasSize(databaseSizeBeforeCreate + 1);
    BranchVaccineAvailability testBranchVaccineAvailability = branchVaccineAvailabilityList.get(
      branchVaccineAvailabilityList.size() - 1
    );
    assertThat(testBranchVaccineAvailability.getVaccinationDateTime())
      .isEqualTo(DEFAULT_VACCINATION_DATE_TIME);
    assertThat(testBranchVaccineAvailability.getDuration())
      .isEqualTo(DEFAULT_DURATION);
    assertThat(testBranchVaccineAvailability.getAvailableSeats())
      .isEqualTo(DEFAULT_AVAILABLE_SEATS);
  }

  @Test
  @Transactional
  void createBranchVaccineAvailabilityWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = branchVaccineAvailabilityRepository
      .findAll()
      .size();

    // Create the BranchVaccineAvailability with an existing ID
    branchVaccineAvailability.setId(1L);
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );

    // An entity with an existing ID cannot be created, so this API call must fail
    restBranchVaccineAvailabilityMockMvc
      .perform(
        post("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isBadRequest());

    // Validate the BranchVaccineAvailability in the database
    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  void checkVaccinationDateTimeIsRequired() throws Exception {
    int databaseSizeBeforeTest = branchVaccineAvailabilityRepository
      .findAll()
      .size();
    // set the field null
    branchVaccineAvailability.setVaccinationDateTime(null);

    // Create the BranchVaccineAvailability, which fails.
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );

    restBranchVaccineAvailabilityMockMvc
      .perform(
        post("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isBadRequest());

    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void checkDurationIsRequired() throws Exception {
    int databaseSizeBeforeTest = branchVaccineAvailabilityRepository
      .findAll()
      .size();
    // set the field null
    branchVaccineAvailability.setDuration(null);

    // Create the BranchVaccineAvailability, which fails.
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );

    restBranchVaccineAvailabilityMockMvc
      .perform(
        post("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isBadRequest());

    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void checkAvailableSeatsIsRequired() throws Exception {
    int databaseSizeBeforeTest = branchVaccineAvailabilityRepository
      .findAll()
      .size();
    // set the field null
    branchVaccineAvailability.setAvailableSeats(null);

    // Create the BranchVaccineAvailability, which fails.
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );

    restBranchVaccineAvailabilityMockMvc
      .perform(
        post("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isBadRequest());

    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void getAllBranchVaccineAvailabilities() throws Exception {
    // Initialize the database
    branchVaccineAvailabilityRepository.saveAndFlush(branchVaccineAvailability);

    // Get all the branchVaccineAvailabilityList
    restBranchVaccineAvailabilityMockMvc
      .perform(get("/api/branch-vaccine-availabilities?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.[*].id")
          .value(hasItem(branchVaccineAvailability.getId().intValue()))
      )
      .andExpect(
        jsonPath("$.[*].vaccinationDateTime")
          .value(hasItem(sameInstant(DEFAULT_VACCINATION_DATE_TIME)))
      )
      .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
      .andExpect(
        jsonPath("$.[*].availableSeats").value(hasItem(DEFAULT_AVAILABLE_SEATS))
      );
  }

  @Test
  @Transactional
  void getBranchVaccineAvailability() throws Exception {
    // Initialize the database
    branchVaccineAvailabilityRepository.saveAndFlush(branchVaccineAvailability);

    // Get the branchVaccineAvailability
    restBranchVaccineAvailabilityMockMvc
      .perform(
        get(
          "/api/branch-vaccine-availabilities/{id}",
          branchVaccineAvailability.getId()
        )
      )
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.id").value(branchVaccineAvailability.getId().intValue())
      )
      .andExpect(
        jsonPath("$.vaccinationDateTime")
          .value(sameInstant(DEFAULT_VACCINATION_DATE_TIME))
      )
      .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION))
      .andExpect(jsonPath("$.availableSeats").value(DEFAULT_AVAILABLE_SEATS));
  }

  @Test
  @Transactional
  void getNonExistingBranchVaccineAvailability() throws Exception {
    // Get the branchVaccineAvailability
    restBranchVaccineAvailabilityMockMvc
      .perform(get("/api/branch-vaccine-availabilities/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  void updateBranchVaccineAvailability() throws Exception {
    // Initialize the database
    branchVaccineAvailabilityRepository.saveAndFlush(branchVaccineAvailability);

    int databaseSizeBeforeUpdate = branchVaccineAvailabilityRepository
      .findAll()
      .size();

    // Update the branchVaccineAvailability
    BranchVaccineAvailability updatedBranchVaccineAvailability = branchVaccineAvailabilityRepository
      .findById(branchVaccineAvailability.getId())
      .get();
    // Disconnect from session so that the updates on updatedBranchVaccineAvailability are not directly saved in db
    em.detach(updatedBranchVaccineAvailability);
    updatedBranchVaccineAvailability
      .vaccinationDateTime(UPDATED_VACCINATION_DATE_TIME)
      .duration(UPDATED_DURATION)
      .availableSeats(UPDATED_AVAILABLE_SEATS);
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      updatedBranchVaccineAvailability
    );

    restBranchVaccineAvailabilityMockMvc
      .perform(
        put("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isOk());

    // Validate the BranchVaccineAvailability in the database
    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeUpdate);
    BranchVaccineAvailability testBranchVaccineAvailability = branchVaccineAvailabilityList.get(
      branchVaccineAvailabilityList.size() - 1
    );
    assertThat(testBranchVaccineAvailability.getVaccinationDateTime())
      .isEqualTo(UPDATED_VACCINATION_DATE_TIME);
    assertThat(testBranchVaccineAvailability.getDuration())
      .isEqualTo(UPDATED_DURATION);
    assertThat(testBranchVaccineAvailability.getAvailableSeats())
      .isEqualTo(UPDATED_AVAILABLE_SEATS);
  }

  @Test
  @Transactional
  void updateNonExistingBranchVaccineAvailability() throws Exception {
    int databaseSizeBeforeUpdate = branchVaccineAvailabilityRepository
      .findAll()
      .size();

    // Create the BranchVaccineAvailability
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO = branchVaccineAvailabilityMapper.toDto(
      branchVaccineAvailability
    );

    // If the entity doesn't have an ID, it will throw BadRequestAlertException
    restBranchVaccineAvailabilityMockMvc
      .perform(
        put("/api/branch-vaccine-availabilities")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(branchVaccineAvailabilityDTO)
          )
      )
      .andExpect(status().isBadRequest());

    // Validate the BranchVaccineAvailability in the database
    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList).hasSize(databaseSizeBeforeUpdate);
  }

  @Test
  @Transactional
  void deleteBranchVaccineAvailability() throws Exception {
    // Initialize the database
    branchVaccineAvailabilityRepository.saveAndFlush(branchVaccineAvailability);

    int databaseSizeBeforeDelete = branchVaccineAvailabilityRepository
      .findAll()
      .size();

    // Delete the branchVaccineAvailability
    restBranchVaccineAvailabilityMockMvc
      .perform(
        delete(
          "/api/branch-vaccine-availabilities/{id}",
          branchVaccineAvailability.getId()
        )
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate the database contains one less item
    List<BranchVaccineAvailability> branchVaccineAvailabilityList = branchVaccineAvailabilityRepository.findAll();
    assertThat(branchVaccineAvailabilityList)
      .hasSize(databaseSizeBeforeDelete - 1);
  }
}
