package eg.a1softech.vaccnow.web.rest;

import static eg.a1softech.vaccnow.web.rest.util.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eg.a1softech.vaccnow.VaccnowApplication;
import eg.a1softech.vaccnow.domain.PatientVaccineAppointment;
import eg.a1softech.vaccnow.domain.enumeration.PaymentMethod;
import eg.a1softech.vaccnow.repository.PatientVaccineAppointmentRepository;
import eg.a1softech.vaccnow.service.PatientVaccineAppointmentService;
import eg.a1softech.vaccnow.service.dto.PatientVaccineAppointmentDTO;
import eg.a1softech.vaccnow.service.mapper.PatientVaccineAppointmentMapper;
import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PatientVaccineAppointmentResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@SpringBootTest(classes = VaccnowApplication.class)
@AutoConfigureMockMvc
class PatientVaccineAppointmentResourceITest {

  private static final PaymentMethod DEFAULT_PAYMENT_METHOD =
    PaymentMethod.CASH;
  private static final PaymentMethod UPDATED_PAYMENT_METHOD =
    PaymentMethod.CREDIT;

  private static final ZonedDateTime DEFAULT_VACCINATION_DATE_TIME = ZonedDateTime.ofInstant(
    Instant.ofEpochMilli(0L),
    ZoneOffset.UTC
  );
  private static final ZonedDateTime UPDATED_VACCINATION_DATE_TIME = ZonedDateTime
    .now(ZoneId.systemDefault())
    .withNano(0);

  private static final String DEFAULT_FAWRY_TRANSACTION_ID = "AAAAAAAAAA";
  private static final String UPDATED_FAWRY_TRANSACTION_ID = "BBBBBBBBBB";

  private static final String DEFAULT_CREDIT_CARD_LAST_4_DIGITS = "AAAAAAAAAA";
  private static final String UPDATED_CREDIT_CARD_LAST_4_DIGITS = "BBBBBBBBBB";

  private static final BigDecimal DEFAULT_AMOUNT_PAID = new BigDecimal(1);
  private static final BigDecimal UPDATED_AMOUNT_PAID = new BigDecimal(2);

  private static final Boolean DEFAULT_CONFIRMED = false;
  private static final Boolean UPDATED_CONFIRMED = true;

  private static final Boolean DEFAULT_TAKEN = false;
  private static final Boolean UPDATED_TAKEN = true;

  @Autowired
  private PatientVaccineAppointmentRepository patientVaccineAppointmentRepository;

  @Autowired
  private PatientVaccineAppointmentMapper patientVaccineAppointmentMapper;

  @Autowired
  private PatientVaccineAppointmentService patientVaccineAppointmentService;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restPatientVaccineAppointmentMockMvc;

  private PatientVaccineAppointment patientVaccineAppointment;

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static PatientVaccineAppointment createEntity(EntityManager em) {
    PatientVaccineAppointment patientVaccineAppointment = new PatientVaccineAppointment()
      .paymentMethod(DEFAULT_PAYMENT_METHOD)
      .vaccinationDateTime(DEFAULT_VACCINATION_DATE_TIME)
      .fawryTransactionId(DEFAULT_FAWRY_TRANSACTION_ID)
      .creditCardLast4Digits(DEFAULT_CREDIT_CARD_LAST_4_DIGITS)
      .amountPaid(DEFAULT_AMOUNT_PAID)
      .confirmed(DEFAULT_CONFIRMED)
      .taken(DEFAULT_TAKEN);
    return patientVaccineAppointment;
  }

  /**
   * Create an updated entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static PatientVaccineAppointment createUpdatedEntity(
    EntityManager em
  ) {
    PatientVaccineAppointment patientVaccineAppointment = new PatientVaccineAppointment()
      .paymentMethod(UPDATED_PAYMENT_METHOD)
      .vaccinationDateTime(UPDATED_VACCINATION_DATE_TIME)
      .fawryTransactionId(UPDATED_FAWRY_TRANSACTION_ID)
      .creditCardLast4Digits(UPDATED_CREDIT_CARD_LAST_4_DIGITS)
      .amountPaid(UPDATED_AMOUNT_PAID)
      .confirmed(UPDATED_CONFIRMED)
      .taken(UPDATED_TAKEN);
    return patientVaccineAppointment;
  }

  @BeforeEach
  public void initTest() {
    patientVaccineAppointment = createEntity(em);
  }

  @Test
  @Transactional
  void createPatientVaccineAppointment() throws Exception {
    int databaseSizeBeforeCreate = patientVaccineAppointmentRepository
      .findAll()
      .size();
    // Create the PatientVaccineAppointment
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      patientVaccineAppointment
    );
    restPatientVaccineAppointmentMockMvc
      .perform(
        post("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isCreated());

    // Validate the PatientVaccineAppointment in the database
    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList)
      .hasSize(databaseSizeBeforeCreate + 1);
    PatientVaccineAppointment testPatientVaccineAppointment = patientVaccineAppointmentList.get(
      patientVaccineAppointmentList.size() - 1
    );
    assertThat(testPatientVaccineAppointment.getPaymentMethod())
      .isEqualTo(DEFAULT_PAYMENT_METHOD);
    assertThat(testPatientVaccineAppointment.getVaccinationDateTime())
      .isEqualTo(DEFAULT_VACCINATION_DATE_TIME);
    assertThat(testPatientVaccineAppointment.getFawryTransactionId())
      .isEqualTo(DEFAULT_FAWRY_TRANSACTION_ID);
    assertThat(testPatientVaccineAppointment.getCreditCardLast4Digits())
      .isEqualTo(DEFAULT_CREDIT_CARD_LAST_4_DIGITS);
    assertThat(testPatientVaccineAppointment.getAmountPaid())
      .isEqualTo(DEFAULT_AMOUNT_PAID);
    assertThat(testPatientVaccineAppointment.isConfirmed())
      .isEqualTo(DEFAULT_CONFIRMED);
    assertThat(testPatientVaccineAppointment.isTaken())
      .isEqualTo(DEFAULT_TAKEN);
  }

  @Test
  @Transactional
  void createPatientVaccineAppointmentWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = patientVaccineAppointmentRepository
      .findAll()
      .size();

    // Create the PatientVaccineAppointment with an existing ID
    patientVaccineAppointment.setId(1L);
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      patientVaccineAppointment
    );

    // An entity with an existing ID cannot be created, so this API call must fail
    restPatientVaccineAppointmentMockMvc
      .perform(
        post("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isBadRequest());

    // Validate the PatientVaccineAppointment in the database
    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  void checkPaymentMethodIsRequired() throws Exception {
    int databaseSizeBeforeTest = patientVaccineAppointmentRepository
      .findAll()
      .size();
    // set the field null
    patientVaccineAppointment.setPaymentMethod(null);

    // Create the PatientVaccineAppointment, which fails.
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      patientVaccineAppointment
    );

    restPatientVaccineAppointmentMockMvc
      .perform(
        post("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isBadRequest());

    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void checkVaccinationDateTimeIsRequired() throws Exception {
    int databaseSizeBeforeTest = patientVaccineAppointmentRepository
      .findAll()
      .size();
    // set the field null
    patientVaccineAppointment.setVaccinationDateTime(null);

    // Create the PatientVaccineAppointment, which fails.
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      patientVaccineAppointment
    );

    restPatientVaccineAppointmentMockMvc
      .perform(
        post("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isBadRequest());

    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void getAllPatientVaccineAppointments() throws Exception {
    // Initialize the database
    patientVaccineAppointmentRepository.saveAndFlush(patientVaccineAppointment);

    // Get all the patientVaccineAppointmentList
    restPatientVaccineAppointmentMockMvc
      .perform(get("/api/patient-vaccine-appointments?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.[*].id")
          .value(hasItem(patientVaccineAppointment.getId().intValue()))
      )
      .andExpect(
        jsonPath("$.[*].paymentMethod")
          .value(hasItem(DEFAULT_PAYMENT_METHOD.toString()))
      )
      .andExpect(
        jsonPath("$.[*].vaccinationDateTime")
          .value(hasItem(sameInstant(DEFAULT_VACCINATION_DATE_TIME)))
      )
      .andExpect(
        jsonPath("$.[*].fawryTransactionId")
          .value(hasItem(DEFAULT_FAWRY_TRANSACTION_ID))
      )
      .andExpect(
        jsonPath("$.[*].creditCardLast4Digits")
          .value(hasItem(DEFAULT_CREDIT_CARD_LAST_4_DIGITS))
      )
      .andExpect(
        jsonPath("$.[*].amountPaid")
          .value(hasItem(DEFAULT_AMOUNT_PAID.intValue()))
      )
      .andExpect(
        jsonPath("$.[*].confirmed")
          .value(hasItem(DEFAULT_CONFIRMED.booleanValue()))
      )
      .andExpect(
        jsonPath("$.[*].taken").value(hasItem(DEFAULT_TAKEN.booleanValue()))
      );
  }

  @Test
  @Transactional
  void getPatientVaccineAppointment() throws Exception {
    // Initialize the database
    patientVaccineAppointmentRepository.saveAndFlush(patientVaccineAppointment);

    // Get the patientVaccineAppointment
    restPatientVaccineAppointmentMockMvc
      .perform(
        get(
          "/api/patient-vaccine-appointments/{id}",
          patientVaccineAppointment.getId()
        )
      )
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.id").value(patientVaccineAppointment.getId().intValue())
      )
      .andExpect(
        jsonPath("$.paymentMethod").value(DEFAULT_PAYMENT_METHOD.toString())
      )
      .andExpect(
        jsonPath("$.vaccinationDateTime")
          .value(sameInstant(DEFAULT_VACCINATION_DATE_TIME))
      )
      .andExpect(
        jsonPath("$.fawryTransactionId").value(DEFAULT_FAWRY_TRANSACTION_ID)
      )
      .andExpect(
        jsonPath("$.creditCardLast4Digits")
          .value(DEFAULT_CREDIT_CARD_LAST_4_DIGITS)
      )
      .andExpect(jsonPath("$.amountPaid").value(DEFAULT_AMOUNT_PAID.intValue()))
      .andExpect(
        jsonPath("$.confirmed").value(DEFAULT_CONFIRMED.booleanValue())
      )
      .andExpect(jsonPath("$.taken").value(DEFAULT_TAKEN.booleanValue()));
  }

  @Test
  @Transactional
  void getNonExistingPatientVaccineAppointment() throws Exception {
    // Get the patientVaccineAppointment
    restPatientVaccineAppointmentMockMvc
      .perform(get("/api/patient-vaccine-appointments/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  void updatePatientVaccineAppointment() throws Exception {
    // Initialize the database
    patientVaccineAppointmentRepository.saveAndFlush(patientVaccineAppointment);

    int databaseSizeBeforeUpdate = patientVaccineAppointmentRepository
      .findAll()
      .size();

    // Update the patientVaccineAppointment
    PatientVaccineAppointment updatedPatientVaccineAppointment = patientVaccineAppointmentRepository
      .findById(patientVaccineAppointment.getId())
      .get();
    // Disconnect from session so that the updates on updatedPatientVaccineAppointment are not directly saved in db
    em.detach(updatedPatientVaccineAppointment);
    updatedPatientVaccineAppointment
      .paymentMethod(UPDATED_PAYMENT_METHOD)
      .vaccinationDateTime(UPDATED_VACCINATION_DATE_TIME)
      .fawryTransactionId(UPDATED_FAWRY_TRANSACTION_ID)
      .creditCardLast4Digits(UPDATED_CREDIT_CARD_LAST_4_DIGITS)
      .amountPaid(UPDATED_AMOUNT_PAID)
      .confirmed(UPDATED_CONFIRMED)
      .taken(UPDATED_TAKEN);
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      updatedPatientVaccineAppointment
    );

    restPatientVaccineAppointmentMockMvc
      .perform(
        put("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isOk());

    // Validate the PatientVaccineAppointment in the database
    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList).hasSize(databaseSizeBeforeUpdate);
    PatientVaccineAppointment testPatientVaccineAppointment = patientVaccineAppointmentList.get(
      patientVaccineAppointmentList.size() - 1
    );
    assertThat(testPatientVaccineAppointment.getPaymentMethod())
      .isEqualTo(UPDATED_PAYMENT_METHOD);
    assertThat(testPatientVaccineAppointment.getVaccinationDateTime())
      .isEqualTo(UPDATED_VACCINATION_DATE_TIME);
    assertThat(testPatientVaccineAppointment.getFawryTransactionId())
      .isEqualTo(UPDATED_FAWRY_TRANSACTION_ID);
    assertThat(testPatientVaccineAppointment.getCreditCardLast4Digits())
      .isEqualTo(UPDATED_CREDIT_CARD_LAST_4_DIGITS);
    assertThat(testPatientVaccineAppointment.getAmountPaid())
      .isEqualTo(UPDATED_AMOUNT_PAID);
    assertThat(testPatientVaccineAppointment.isConfirmed())
      .isEqualTo(UPDATED_CONFIRMED);
    assertThat(testPatientVaccineAppointment.isTaken())
      .isEqualTo(UPDATED_TAKEN);
  }

  @Test
  @Transactional
  void updateNonExistingPatientVaccineAppointment() throws Exception {
    int databaseSizeBeforeUpdate = patientVaccineAppointmentRepository
      .findAll()
      .size();

    // Create the PatientVaccineAppointment
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO = patientVaccineAppointmentMapper.toDto(
      patientVaccineAppointment
    );

    // If the entity doesn't have an ID, it will throw BadRequestAlertException
    restPatientVaccineAppointmentMockMvc
      .perform(
        put("/api/patient-vaccine-appointments")
          .contentType(MediaType.APPLICATION_JSON)
          .content(
            TestUtil.convertObjectToJsonBytes(patientVaccineAppointmentDTO)
          )
      )
      .andExpect(status().isBadRequest());

    // Validate the PatientVaccineAppointment in the database
    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList).hasSize(databaseSizeBeforeUpdate);
  }

  @Test
  @Transactional
  void deletePatientVaccineAppointment() throws Exception {
    // Initialize the database
    patientVaccineAppointmentRepository.saveAndFlush(patientVaccineAppointment);

    int databaseSizeBeforeDelete = patientVaccineAppointmentRepository
      .findAll()
      .size();

    // Delete the patientVaccineAppointment
    restPatientVaccineAppointmentMockMvc
      .perform(
        delete(
          "/api/patient-vaccine-appointments/{id}",
          patientVaccineAppointment.getId()
        )
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate the database contains one less item
    List<PatientVaccineAppointment> patientVaccineAppointmentList = patientVaccineAppointmentRepository.findAll();
    assertThat(patientVaccineAppointmentList)
      .hasSize(databaseSizeBeforeDelete - 1);
  }
}
