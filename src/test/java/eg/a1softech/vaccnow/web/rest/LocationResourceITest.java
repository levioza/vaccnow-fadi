package eg.a1softech.vaccnow.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eg.a1softech.vaccnow.VaccnowApplication;
import eg.a1softech.vaccnow.domain.Location;
import eg.a1softech.vaccnow.repository.LocationRepository;
import eg.a1softech.vaccnow.service.LocationService;
import eg.a1softech.vaccnow.service.dto.LocationDTO;
import eg.a1softech.vaccnow.service.mapper.LocationMapper;
import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link LocationResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@SpringBootTest(classes = VaccnowApplication.class)
@AutoConfigureMockMvc
class LocationResourceITest {

  private static final String DEFAULT_STREET_ADDRESS = "AAAAAAAAAA";
  private static final String UPDATED_STREET_ADDRESS = "BBBBBBBBBB";

  private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
  private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

  private static final String DEFAULT_CITY = "AAAAAAAAAA";
  private static final String UPDATED_CITY = "BBBBBBBBBB";

  @Autowired
  private LocationRepository locationRepository;

  @Autowired
  private LocationMapper locationMapper;

  @Autowired
  private LocationService locationService;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restLocationMockMvc;

  private Location location;

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Location createEntity(EntityManager em) {
    Location location = new Location()
      .streetAddress(DEFAULT_STREET_ADDRESS)
      .postalCode(DEFAULT_POSTAL_CODE)
      .city(DEFAULT_CITY);
    return location;
  }

  /**
   * Create an updated entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Location createUpdatedEntity(EntityManager em) {
    Location location = new Location()
      .streetAddress(UPDATED_STREET_ADDRESS)
      .postalCode(UPDATED_POSTAL_CODE)
      .city(UPDATED_CITY);
    return location;
  }

  @BeforeEach
  public void initTest() {
    location = createEntity(em);
  }

  @Test
  @Transactional
  void createLocation() throws Exception {
    int databaseSizeBeforeCreate = locationRepository.findAll().size();
    // Create the Location
    LocationDTO locationDTO = locationMapper.toDto(location);
    restLocationMockMvc
      .perform(
        post("/api/locations")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(locationDTO))
      )
      .andExpect(status().isCreated());

    // Validate the Location in the database
    List<Location> locationList = locationRepository.findAll();
    assertThat(locationList).hasSize(databaseSizeBeforeCreate + 1);
    Location testLocation = locationList.get(locationList.size() - 1);
    assertThat(testLocation.getStreetAddress())
      .isEqualTo(DEFAULT_STREET_ADDRESS);
    assertThat(testLocation.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
    assertThat(testLocation.getCity()).isEqualTo(DEFAULT_CITY);
  }

  @Test
  @Transactional
  void createLocationWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = locationRepository.findAll().size();

    // Create the Location with an existing ID
    location.setId(1L);
    LocationDTO locationDTO = locationMapper.toDto(location);

    // An entity with an existing ID cannot be created, so this API call must fail
    restLocationMockMvc
      .perform(
        post("/api/locations")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(locationDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Location in the database
    List<Location> locationList = locationRepository.findAll();
    assertThat(locationList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  void getAllLocations() throws Exception {
    // Initialize the database
    locationRepository.saveAndFlush(location);

    // Get all the locationList
    restLocationMockMvc
      .perform(get("/api/locations?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.[*].id").value(hasItem(location.getId().intValue()))
      )
      .andExpect(
        jsonPath("$.[*].streetAddress").value(hasItem(DEFAULT_STREET_ADDRESS))
      )
      .andExpect(
        jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE))
      )
      .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)));
  }

  @Test
  @Transactional
  void getLocation() throws Exception {
    // Initialize the database
    locationRepository.saveAndFlush(location);

    // Get the location
    restLocationMockMvc
      .perform(get("/api/locations/{id}", location.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(location.getId().intValue()))
      .andExpect(jsonPath("$.streetAddress").value(DEFAULT_STREET_ADDRESS))
      .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
      .andExpect(jsonPath("$.city").value(DEFAULT_CITY));
  }

  @Test
  @Transactional
  void getNonExistingLocation() throws Exception {
    // Get the location
    restLocationMockMvc
      .perform(get("/api/locations/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  void updateLocation() throws Exception {
    // Initialize the database
    locationRepository.saveAndFlush(location);

    int databaseSizeBeforeUpdate = locationRepository.findAll().size();

    // Update the location
    Location updatedLocation = locationRepository
      .findById(location.getId())
      .get();
    // Disconnect from session so that the updates on updatedLocation are not directly saved in db
    em.detach(updatedLocation);
    updatedLocation
      .streetAddress(UPDATED_STREET_ADDRESS)
      .postalCode(UPDATED_POSTAL_CODE)
      .city(UPDATED_CITY);
    LocationDTO locationDTO = locationMapper.toDto(updatedLocation);

    restLocationMockMvc
      .perform(
        put("/api/locations")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(locationDTO))
      )
      .andExpect(status().isOk());

    // Validate the Location in the database
    List<Location> locationList = locationRepository.findAll();
    assertThat(locationList).hasSize(databaseSizeBeforeUpdate);
    Location testLocation = locationList.get(locationList.size() - 1);
    assertThat(testLocation.getStreetAddress())
      .isEqualTo(UPDATED_STREET_ADDRESS);
    assertThat(testLocation.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
    assertThat(testLocation.getCity()).isEqualTo(UPDATED_CITY);
  }

  @Test
  @Transactional
  void updateNonExistingLocation() throws Exception {
    int databaseSizeBeforeUpdate = locationRepository.findAll().size();

    // Create the Location
    LocationDTO locationDTO = locationMapper.toDto(location);

    // If the entity doesn't have an ID, it will throw BadRequestAlertException
    restLocationMockMvc
      .perform(
        put("/api/locations")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(locationDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Location in the database
    List<Location> locationList = locationRepository.findAll();
    assertThat(locationList).hasSize(databaseSizeBeforeUpdate);
  }

  @Test
  @Transactional
  void deleteLocation() throws Exception {
    // Initialize the database
    locationRepository.saveAndFlush(location);

    int databaseSizeBeforeDelete = locationRepository.findAll().size();

    // Delete the location
    restLocationMockMvc
      .perform(
        delete("/api/locations/{id}", location.getId())
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate the database contains one less item
    List<Location> locationList = locationRepository.findAll();
    assertThat(locationList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
