package eg.a1softech.vaccnow.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eg.a1softech.vaccnow.VaccnowApplication;
import eg.a1softech.vaccnow.domain.Vaccine;
import eg.a1softech.vaccnow.repository.VaccineRepository;
import eg.a1softech.vaccnow.service.VaccineService;
import eg.a1softech.vaccnow.service.dto.VaccineDTO;
import eg.a1softech.vaccnow.service.mapper.VaccineMapper;
import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link VaccineResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@SpringBootTest(classes = VaccnowApplication.class)
@AutoConfigureMockMvc
class VaccineResourceITest {

  private static final String DEFAULT_NAME = "AAAAAAAAAA";
  private static final String UPDATED_NAME = "BBBBBBBBBB";

  @Autowired
  private VaccineRepository vaccineRepository;

  @Autowired
  private VaccineMapper vaccineMapper;

  @Autowired
  private VaccineService vaccineService;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restVaccineMockMvc;

  private Vaccine vaccine;

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Vaccine createEntity(EntityManager em) {
    Vaccine vaccine = new Vaccine().name(DEFAULT_NAME);
    return vaccine;
  }

  /**
   * Create an updated entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Vaccine createUpdatedEntity(EntityManager em) {
    Vaccine vaccine = new Vaccine().name(UPDATED_NAME);
    return vaccine;
  }

  @BeforeEach
  public void initTest() {
    vaccine = createEntity(em);
  }

  @Test
  @Transactional
  void createVaccine() throws Exception {
    int databaseSizeBeforeCreate = vaccineRepository.findAll().size();
    // Create the Vaccine
    VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);
    restVaccineMockMvc
      .perform(
        post("/api/vaccines")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(vaccineDTO))
      )
      .andExpect(status().isCreated());

    // Validate the Vaccine in the database
    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeCreate + 1);
    Vaccine testVaccine = vaccineList.get(vaccineList.size() - 1);
    assertThat(testVaccine.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  void createVaccineWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = vaccineRepository.findAll().size();

    // Create the Vaccine with an existing ID
    vaccine.setId(1L);
    VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);

    // An entity with an existing ID cannot be created, so this API call must fail
    restVaccineMockMvc
      .perform(
        post("/api/vaccines")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(vaccineDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Vaccine in the database
    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = vaccineRepository.findAll().size();
    // set the field null
    vaccine.setName(null);

    // Create the Vaccine, which fails.
    VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);

    restVaccineMockMvc
      .perform(
        post("/api/vaccines")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(vaccineDTO))
      )
      .andExpect(status().isBadRequest());

    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void getAllVaccines() throws Exception {
    // Initialize the database
    vaccineRepository.saveAndFlush(vaccine);

    // Get all the vaccineList
    restVaccineMockMvc
      .perform(get("/api/vaccines?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(
        jsonPath("$.[*].id").value(hasItem(vaccine.getId().intValue()))
      )
      .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
  }

  @Test
  @Transactional
  void getVaccine() throws Exception {
    // Initialize the database
    vaccineRepository.saveAndFlush(vaccine);

    // Get the vaccine
    restVaccineMockMvc
      .perform(get("/api/vaccines/{id}", vaccine.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(vaccine.getId().intValue()))
      .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
  }

  @Test
  @Transactional
  void getNonExistingVaccine() throws Exception {
    // Get the vaccine
    restVaccineMockMvc
      .perform(get("/api/vaccines/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  void updateVaccine() throws Exception {
    // Initialize the database
    vaccineRepository.saveAndFlush(vaccine);

    int databaseSizeBeforeUpdate = vaccineRepository.findAll().size();

    // Update the vaccine
    Vaccine updatedVaccine = vaccineRepository.findById(vaccine.getId()).get();
    // Disconnect from session so that the updates on updatedVaccine are not directly saved in db
    em.detach(updatedVaccine);
    updatedVaccine.name(UPDATED_NAME);
    VaccineDTO vaccineDTO = vaccineMapper.toDto(updatedVaccine);

    restVaccineMockMvc
      .perform(
        put("/api/vaccines")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(vaccineDTO))
      )
      .andExpect(status().isOk());

    // Validate the Vaccine in the database
    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeUpdate);
    Vaccine testVaccine = vaccineList.get(vaccineList.size() - 1);
    assertThat(testVaccine.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  void updateNonExistingVaccine() throws Exception {
    int databaseSizeBeforeUpdate = vaccineRepository.findAll().size();

    // Create the Vaccine
    VaccineDTO vaccineDTO = vaccineMapper.toDto(vaccine);

    // If the entity doesn't have an ID, it will throw BadRequestAlertException
    restVaccineMockMvc
      .perform(
        put("/api/vaccines")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(vaccineDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Vaccine in the database
    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeUpdate);
  }

  @Test
  @Transactional
  void deleteVaccine() throws Exception {
    // Initialize the database
    vaccineRepository.saveAndFlush(vaccine);

    int databaseSizeBeforeDelete = vaccineRepository.findAll().size();

    // Delete the vaccine
    restVaccineMockMvc
      .perform(
        delete("/api/vaccines/{id}", vaccine.getId())
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate the database contains one less item
    List<Vaccine> vaccineList = vaccineRepository.findAll();
    assertThat(vaccineList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
