/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eg.a1softech.vaccnow.VaccnowApplication;
import eg.a1softech.vaccnow.domain.Branch;
import eg.a1softech.vaccnow.repository.BranchRepository;
import eg.a1softech.vaccnow.service.BranchService;
import eg.a1softech.vaccnow.service.dto.BranchDTO;
import eg.a1softech.vaccnow.service.mapper.BranchMapper;
import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BranchResource} REST controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@SpringBootTest(classes = VaccnowApplication.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
class BranchResourceITest {

  private static final String DEFAULT_NAME = "AAAAAAAAAA";
  private static final String UPDATED_NAME = "BBBBBBBBBB";

  @Autowired
  private BranchRepository branchRepository;

  @Mock
  private BranchRepository branchRepositoryMock;

  @Autowired
  private BranchMapper branchMapper;

  @Mock
  private BranchService branchServiceMock;

  @Autowired
  private BranchService branchService;

  @Autowired
  private EntityManager em;

  @Autowired
  private MockMvc restBranchMockMvc;

  private Branch branch;

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Branch createEntity(EntityManager em) {
    Branch branch = new Branch().name(DEFAULT_NAME);
    return branch;
  }

  /**
   * Create an updated entity for this test.
   *
   * This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.
   */
  public static Branch createUpdatedEntity(EntityManager em) {
    Branch branch = new Branch().name(UPDATED_NAME);
    return branch;
  }

  @BeforeEach
  public void initTest() {
    branch = createEntity(em);
  }

  @Test
  @Transactional
  void createBranch() throws Exception {
    int databaseSizeBeforeCreate = branchRepository.findAll().size();
    // Create the Branch
    BranchDTO branchDTO = branchMapper.toDto(branch);
    restBranchMockMvc
      .perform(
        post("/api/branches")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(branchDTO))
      )
      .andExpect(status().isCreated());

    // Validate the Branch in the database
    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeCreate + 1);
    Branch testBranch = branchList.get(branchList.size() - 1);
    assertThat(testBranch.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  void createBranchWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = branchRepository.findAll().size();

    // Create the Branch with an existing ID
    branch.setId(1L);
    BranchDTO branchDTO = branchMapper.toDto(branch);

    // An entity with an existing ID cannot be created, so this API call must fail
    restBranchMockMvc
      .perform(
        post("/api/branches")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(branchDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Branch in the database
    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = branchRepository.findAll().size();
    // set the field null
    branch.setName(null);

    // Create the Branch, which fails.
    BranchDTO branchDTO = branchMapper.toDto(branch);

    restBranchMockMvc
      .perform(
        post("/api/branches")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(branchDTO))
      )
      .andExpect(status().isBadRequest());

    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  void getAllBranches() throws Exception {
    // Initialize the database
    branchRepository.saveAndFlush(branch);

    // Get all the branchList
    restBranchMockMvc
      .perform(get("/api/branches?sort=id,desc"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.[*].id").value(hasItem(branch.getId().intValue())))
      .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
  }

  @Test
  @Transactional
  void getBranch() throws Exception {
    // Initialize the database
    branchRepository.saveAndFlush(branch);

    // Get the branch
    restBranchMockMvc
      .perform(get("/api/branches/{id}", branch.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(branch.getId().intValue()))
      .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
  }

  @Test
  @Transactional
  void getNonExistingBranch() throws Exception {
    // Get the branch
    restBranchMockMvc
      .perform(get("/api/branches/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  void updateBranch() throws Exception {
    // Initialize the database
    branchRepository.saveAndFlush(branch);

    int databaseSizeBeforeUpdate = branchRepository.findAll().size();

    // Update the branch
    Branch updatedBranch = branchRepository.findById(branch.getId()).get();
    // Disconnect from session so that the updates on updatedBranch are not directly saved in db
    em.detach(updatedBranch);
    updatedBranch.name(UPDATED_NAME);
    BranchDTO branchDTO = branchMapper.toDto(updatedBranch);

    restBranchMockMvc
      .perform(
        put("/api/branches")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(branchDTO))
      )
      .andExpect(status().isOk());

    // Validate the Branch in the database
    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeUpdate);
    Branch testBranch = branchList.get(branchList.size() - 1);
    assertThat(testBranch.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  void updateNonExistingBranch() throws Exception {
    int databaseSizeBeforeUpdate = branchRepository.findAll().size();

    // Create the Branch
    BranchDTO branchDTO = branchMapper.toDto(branch);

    // If the entity doesn't have an ID, it will throw BadRequestAlertException
    restBranchMockMvc
      .perform(
        put("/api/branches")
          .contentType(MediaType.APPLICATION_JSON)
          .content(TestUtil.convertObjectToJsonBytes(branchDTO))
      )
      .andExpect(status().isBadRequest());

    // Validate the Branch in the database
    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeUpdate);
  }

  @Test
  @Transactional
  void deleteBranch() throws Exception {
    // Initialize the database
    branchRepository.saveAndFlush(branch);

    int databaseSizeBeforeDelete = branchRepository.findAll().size();

    // Delete the branch
    restBranchMockMvc
      .perform(
        delete("/api/branches/{id}", branch.getId())
          .accept(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isNoContent());

    // Validate the database contains one less item
    List<Branch> branchList = branchRepository.findAll();
    assertThat(branchList).hasSize(databaseSizeBeforeDelete - 1);
  }
}
