/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import org.junit.jupiter.api.Test;

/**
 * PatientVaccineAppointment DTO Test
 *
 * @author Fadi William Ghali Abdelmessih.
 */
class PatientVaccineAppointmentDTOTest {

  @Test
  void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(PatientVaccineAppointmentDTO.class);
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO1 = new PatientVaccineAppointmentDTO();
    patientVaccineAppointmentDTO1.setId(1L);
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO2 = new PatientVaccineAppointmentDTO();
    assertThat(patientVaccineAppointmentDTO1)
      .isNotEqualTo(patientVaccineAppointmentDTO2);
    patientVaccineAppointmentDTO2.setId(patientVaccineAppointmentDTO1.getId());
    assertThat(patientVaccineAppointmentDTO1)
      .isEqualTo(patientVaccineAppointmentDTO2);
    patientVaccineAppointmentDTO2.setId(2L);
    assertThat(patientVaccineAppointmentDTO1)
      .isNotEqualTo(patientVaccineAppointmentDTO2);
    patientVaccineAppointmentDTO1.setId(null);
    assertThat(patientVaccineAppointmentDTO1)
      .isNotEqualTo(patientVaccineAppointmentDTO2);
  }
}
