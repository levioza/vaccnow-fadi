/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import org.junit.jupiter.api.Test;

/**
 * Vaccine DTO Test
 *
 * @author Fadi William Ghali Abdelmessih.
 */
class VaccineDTOTest {

  @Test
  void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(VaccineDTO.class);
    VaccineDTO vaccineDTO1 = new VaccineDTO();
    vaccineDTO1.setId(1L);
    VaccineDTO vaccineDTO2 = new VaccineDTO();
    assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
    vaccineDTO2.setId(vaccineDTO1.getId());
    assertThat(vaccineDTO1).isEqualTo(vaccineDTO2);
    vaccineDTO2.setId(2L);
    assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
    vaccineDTO1.setId(null);
    assertThat(vaccineDTO1).isNotEqualTo(vaccineDTO2);
  }
}
