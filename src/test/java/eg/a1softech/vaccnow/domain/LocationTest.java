/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eg.a1softech.vaccnow.web.rest.util.TestUtil;
import org.junit.jupiter.api.Test;

/**
 * Location entity test.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
class LocationTest {

  @Test
  void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(Location.class);
    Location location1 = new Location();
    location1.setId(1L);
    Location location2 = new Location();
    location2.setId(location1.getId());
    assertThat(location1).isEqualTo(location2);
    location2.setId(2L);
    assertThat(location1).isNotEqualTo(location2);
    location1.setId(null);
    assertThat(location1).isNotEqualTo(location2);
  }
}
