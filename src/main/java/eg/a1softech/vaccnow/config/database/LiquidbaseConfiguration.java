package eg.a1softech.vaccnow.config.database;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@EnableConfigurationProperties(LiquibaseProperties.class)
public class LiquidbaseConfiguration {

  private final Logger log = LoggerFactory.getLogger(
    LiquidbaseConfiguration.class
  );

  private final Environment env;

  public LiquidbaseConfiguration(Environment env) {
    this.env = env;
  }

  @Bean
  public SpringLiquibase liquibase(
    @LiquibaseDataSource ObjectProvider<DataSource> liquibaseDataSource,
    LiquibaseProperties liquibaseProperties,
    ObjectProvider<DataSource> dataSource,
    DataSourceProperties dataSourceProperties
  ) {
    SpringLiquibase liquibase = SpringLiquibaseUtil.createSpringLiquibase(
      liquibaseDataSource.getIfAvailable(),
      liquibaseProperties,
      dataSource.getIfUnique(),
      dataSourceProperties
    );
    liquibase.setChangeLog("classpath:config/liquibase/master.xml");
    liquibase.setContexts(liquibaseProperties.getContexts());
    liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
    liquibase.setLiquibaseSchema(liquibaseProperties.getLiquibaseSchema());
    liquibase.setLiquibaseTablespace(
      liquibaseProperties.getLiquibaseTablespace()
    );
    liquibase.setDatabaseChangeLogLockTable(
      liquibaseProperties.getDatabaseChangeLogLockTable()
    );
    liquibase.setDatabaseChangeLogTable(
      liquibaseProperties.getDatabaseChangeLogTable()
    );
    liquibase.setDropFirst(liquibaseProperties.isDropFirst());
    liquibase.setLabels(liquibaseProperties.getLabels());
    liquibase.setChangeLogParameters(liquibaseProperties.getParameters());
    liquibase.setRollbackFile(liquibaseProperties.getRollbackFile());
    liquibase.setTestRollbackOnUpdate(
      liquibaseProperties.isTestRollbackOnUpdate()
    );
    liquibase.setShouldRun(liquibaseProperties.isEnabled());
    log.debug("Configuring Liquibase");

    return liquibase;
  }
}
