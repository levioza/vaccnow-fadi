package eg.a1softech.vaccnow.config.cors;

import eg.a1softech.vaccnow.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfiguration implements WebMvcConfigurer {

  private static final Logger log = LoggerFactory.getLogger(
    CorsConfiguration.class
  );

  private final ApplicationProperties applicationProperties;

  public CorsConfiguration(ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    log.debug("Configuring CORS");

    var allowCredentials = applicationProperties
      .getCors()
      .getAllowCredentials();
    var maxAge = applicationProperties.getCors().getMaxAge();

    if (allowCredentials == null) {
      allowCredentials = false;
    }

    if (maxAge == null) maxAge = 0L;

    if (applicationProperties.getCors() != null) {
      registry
        .addMapping("/api/**")
        .allowedOrigins(
          String.valueOf(applicationProperties.getCors().getAllowedOrigins())
        )
        .allowedHeaders(
          String.valueOf(applicationProperties.getCors().getAllowedHeaders())
        )
        .exposedHeaders(
          String.valueOf(applicationProperties.getCors().getExposedHeaders())
        )
        .allowCredentials(allowCredentials)
        .maxAge(maxAge);
    }
  }
}
