/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.config.swagger;

import eg.a1softech.vaccnow.config.profile.Constants;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

/**
 * Swagger Configuration.
 *
 * @author Fadi William Ghali Abdelmessih
 */
@Configuration
@Profile(Constants.SPRING_PROFILE_DEVELOPMENT)
public class SwaggerConfiguration {

  private static final Logger log = LoggerFactory.getLogger(
    SwaggerConfiguration.class
  );

  @Value("${server.port}")
  private String serverPort;

  private final Environment env;

  public SwaggerConfiguration(Environment env) {
    this.env = env;
  }

  @Bean
  public OpenAPI customOpenAPI() {
    Server server = new Server();
    server.setUrl("http://localhost:" + serverPort);
    log.debug(
      "Swagger documentation is available on localhost on port {} at /swagger-ui",
      serverPort
    );

    return new OpenAPI()
      .info(
        new Info()
          .title(env.getProperty("spring.application.name"))
          .version("snapshot")
      )
      .servers(Collections.singletonList(server));
  }
}
