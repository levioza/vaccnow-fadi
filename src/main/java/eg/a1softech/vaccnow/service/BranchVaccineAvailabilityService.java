/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service;

import eg.a1softech.vaccnow.service.dto.BranchVaccineAvailabilityDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link eg.a1softech.vaccnow.domain.BranchVaccineAvailability}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public interface BranchVaccineAvailabilityService {
  /**
   * Save a branchVaccineAvailability.
   *
   * @param branchVaccineAvailabilityDTO the entity to save.
   * @return the persisted entity.
   */
  BranchVaccineAvailabilityDTO save(
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO
  );

  /**
   * Get all the branchVaccineAvailabilities.
   *
   * @param pageable the pagination information.
   * @return the list of entities.
   */
  Page<BranchVaccineAvailabilityDTO> findAll(Pageable pageable);

  /**
   * Get all the branchVaccineAvailabilities by branch id.
   *
   * @param pageable the pagination information.
   * @return the list of entities.
   */
  Page<BranchVaccineAvailabilityDTO> findAllByBranchId(
    Pageable pageable,
    Long branchId
  );

  /**
   * Get a specific branch vaccine availability by its id as well as the branch id.
   *
   * @param id the availability id.
   * @param branchId the branch id.
   *
   * @return the list of entities.
   */
  Optional<BranchVaccineAvailabilityDTO> findByIdAndByBranchId(
    Long id,
    Long branchId
  );

  /**
   * Get the "id" branchVaccineAvailability.
   *
   * @param id the id of the entity.
   * @return the entity.
   */
  Optional<BranchVaccineAvailabilityDTO> findOne(Long id);

  /**
   * Delete the "id" branchVaccineAvailability.
   *
   * @param id the id of the entity.
   */
  void delete(Long id);
}
