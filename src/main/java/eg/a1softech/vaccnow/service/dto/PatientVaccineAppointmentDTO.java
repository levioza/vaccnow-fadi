/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.dto;

import eg.a1softech.vaccnow.domain.enumeration.PaymentMethod;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the {@link eg.a1softech.vaccnow.domain.PatientVaccineAppointment} entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public class PatientVaccineAppointmentDTO implements Serializable {

  private Long id;

  @NotNull
  private PaymentMethod paymentMethod;

  @NotNull
  private ZonedDateTime vaccinationDateTime;

  private String fawryTransactionId;

  private String creditCardLast4Digits;

  private BigDecimal amountPaid;

  private Boolean confirmed;

  private Boolean taken;

  private Long branchId;

  private Long patientId;

  private Long vaccineId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public PaymentMethod getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public ZonedDateTime getVaccinationDateTime() {
    return vaccinationDateTime;
  }

  public void setVaccinationDateTime(ZonedDateTime vaccinationDateTime) {
    this.vaccinationDateTime = vaccinationDateTime;
  }

  public String getFawryTransactionId() {
    return fawryTransactionId;
  }

  public void setFawryTransactionId(String fawryTransactionId) {
    this.fawryTransactionId = fawryTransactionId;
  }

  public String getCreditCardLast4Digits() {
    return creditCardLast4Digits;
  }

  public void setCreditCardLast4Digits(String creditCardLast4Digits) {
    this.creditCardLast4Digits = creditCardLast4Digits;
  }

  public BigDecimal getAmountPaid() {
    return amountPaid;
  }

  public void setAmountPaid(BigDecimal amountPaid) {
    this.amountPaid = amountPaid;
  }

  public Boolean isConfirmed() {
    return confirmed;
  }

  public void setConfirmed(Boolean confirmed) {
    this.confirmed = confirmed;
  }

  public Boolean isTaken() {
    return taken;
  }

  public void setTaken(Boolean taken) {
    this.taken = taken;
  }

  public Long getBranchId() {
    return branchId;
  }

  public void setBranchId(Long branchId) {
    this.branchId = branchId;
  }

  public Long getPatientId() {
    return patientId;
  }

  public void setPatientId(Long patientId) {
    this.patientId = patientId;
  }

  public Long getVaccineId() {
    return vaccineId;
  }

  public void setVaccineId(Long vaccineId) {
    this.vaccineId = vaccineId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof PatientVaccineAppointmentDTO)) {
      return false;
    }

    return id != null && id.equals(((PatientVaccineAppointmentDTO) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return (
      "PatientVaccineAppointmentDTO{" +
      "id=" +
      getId() +
      ", paymentMethod='" +
      getPaymentMethod() +
      "'" +
      ", vaccinationDateTime='" +
      getVaccinationDateTime() +
      "'" +
      ", fawryTransactionId='" +
      getFawryTransactionId() +
      "'" +
      ", creditCardLast4Digits='" +
      getCreditCardLast4Digits() +
      "'" +
      ", amountPaid=" +
      getAmountPaid() +
      ", confirmed='" +
      isConfirmed() +
      "'" +
      ", taken='" +
      isTaken() +
      "'" +
      ", branchId=" +
      getBranchId() +
      ", patientId=" +
      getPatientId() +
      ", vaccineId=" +
      getVaccineId() +
      "}"
    );
  }
}
