/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.dto;

import eg.a1softech.vaccnow.domain.enumeration.PaymentMethod;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;

/**
 * Confirm and pay vaccine DTO.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public class ConfirmAndPayVaccineDTO implements Serializable {

  @NotNull
  private Long patientId;

  @NotNull
  private BigDecimal amountPaid;

  @NotNull
  private PaymentMethod paymentMethod;

  private String fawryTransactionId;

  private String creditCardLast4Digits;

  @NotNull
  private Long bvaId;

  public Long getPatientId() {
    return patientId;
  }

  public void setPatientId(Long patientId) {
    this.patientId = patientId;
  }

  public BigDecimal getAmountPaid() {
    return amountPaid;
  }

  public void setAmountPaid(BigDecimal amountPaid) {
    this.amountPaid = amountPaid;
  }

  public PaymentMethod getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public String getFawryTransactionId() {
    return fawryTransactionId;
  }

  public void setFawryTransactionId(String fawryTransactionId) {
    this.fawryTransactionId = fawryTransactionId;
  }

  public String getCreditCardLast4Digits() {
    return creditCardLast4Digits;
  }

  public void setCreditCardLast4Digits(String creditCardLast4Digits) {
    this.creditCardLast4Digits = creditCardLast4Digits;
  }

  public Long getBvaId() {
    return bvaId;
  }

  public void setBvaId(Long bvaId) {
    this.bvaId = bvaId;
  }
}
