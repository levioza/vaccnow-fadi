/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service;

import eg.a1softech.vaccnow.config.ApplicationProperties;
import eg.a1softech.vaccnow.domain.Patient;
import eg.a1softech.vaccnow.domain.PatientVaccineAppointment;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
public class MailService {

  private final Logger log = LoggerFactory.getLogger(MailService.class);

  private static final String PATIENT = "patient";

  private static final String PATIENT_VACCINE_APPOINTEMENT =
    "patientVaccineAppointment";

  private final ApplicationProperties applicationProperties;

  private final JavaMailSender javaMailSender;

  private final SpringTemplateEngine templateEngine;

  public MailService(
    ApplicationProperties applicationProperties,
    JavaMailSender javaMailSender,
    SpringTemplateEngine templateEngine
  ) {
    this.applicationProperties = applicationProperties;
    this.javaMailSender = javaMailSender;
    this.templateEngine = templateEngine;
  }

  @Async
  public void sendEmail(
    String to,
    String subject,
    String content,
    boolean isMultipart,
    boolean isHtml
  ) {
    log.debug(
      "Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
      isMultipart,
      isHtml,
      to,
      subject,
      content
    );

    MimeMessage mimeMessage = javaMailSender.createMimeMessage();
    try {
      MimeMessageHelper message = new MimeMessageHelper(
        mimeMessage,
        isMultipart,
        StandardCharsets.UTF_8.name()
      );
      message.setTo(to);
      message.setFrom(applicationProperties.getMail().getFrom());
      message.setSubject(subject);
      message.setText(content, isHtml);
      javaMailSender.send(mimeMessage);
      log.debug("Sent email to User '{}'", to);
    } catch (MailException | MessagingException e) {
      log.warn("Email could not be sent to user '{}'", to, e);
    }
  }

  @Async
  public void sendPatientVaccineAppointmentConfirmation(
    Patient patient,
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    if (patient.getEmail() == null) {
      log.debug("Email doesn't exist for patient id '{}'", patient.getId());
      return;
    }

    Context context = new Context();
    context.setVariable(PATIENT, patient);
    context.setVariable(
      PATIENT_VACCINE_APPOINTEMENT,
      patientVaccineAppointment
    );
    String subject =
      "Your vaccination appointment has been confirmed at branch " +
      patientVaccineAppointment.getBranch().getName() +
      " on " +
      patientVaccineAppointment.getVaccinationDateTime();
    context.setVariable("title", subject);
    String content = templateEngine.process(
      "mail/patientVaccineAppointmentConfirmation",
      context
    );
    sendEmail(patient.getEmail(), subject, content, false, true);
  }

  @Async
  public void sendThatThePatientVaccineHasBeenTakenConfirmation(
    Patient patient,
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    if (patient.getEmail() == null) {
      log.debug("Email doesn't exist for patient id '{}'", patient.getId());
      return;
    }

    Context context = new Context();
    context.setVariable(PATIENT, patient);
    context.setVariable(
      PATIENT_VACCINE_APPOINTEMENT,
      patientVaccineAppointment
    );
    String subject =
      "Thank you for taking vaccine " +
      patientVaccineAppointment.getVaccine().getName() +
      " at branch " +
      patientVaccineAppointment.getBranch().getName() +
      " on " +
      patientVaccineAppointment.getVaccinationDateTime();
    context.setVariable("title", subject);
    String content = templateEngine.process(
      "mail/patientVaccineHasBeenTakenConfirmation",
      context
    );
    sendEmail(patient.getEmail(), subject, content, false, true);
  }
}
