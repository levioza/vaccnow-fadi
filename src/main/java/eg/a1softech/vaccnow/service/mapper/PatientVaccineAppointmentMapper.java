/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.mapper;

import eg.a1softech.vaccnow.domain.PatientVaccineAppointment;
import eg.a1softech.vaccnow.service.dto.PatientVaccineAppointmentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link PatientVaccineAppointment} and its DTO {@link PatientVaccineAppointmentDTO}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Mapper(
  componentModel = "spring",
  uses = { BranchMapper.class, PatientMapper.class, VaccineMapper.class }
)
public interface PatientVaccineAppointmentMapper
  extends
    EntityMapper<PatientVaccineAppointmentDTO, PatientVaccineAppointment> {
  @Mapping(source = "branch.id", target = "branchId")
  @Mapping(source = "patient.id", target = "patientId")
  @Mapping(source = "vaccine.id", target = "vaccineId")
  PatientVaccineAppointmentDTO toDto(
    PatientVaccineAppointment patientVaccineAppointment
  );

  @Mapping(source = "branchId", target = "branch")
  @Mapping(source = "patientId", target = "patient")
  @Mapping(source = "vaccineId", target = "vaccine")
  PatientVaccineAppointment toEntity(
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO
  );

  default PatientVaccineAppointment fromId(Long id) {
    if (id == null) {
      return null;
    }
    PatientVaccineAppointment patientVaccineAppointment = new PatientVaccineAppointment();
    patientVaccineAppointment.setId(id);
    return patientVaccineAppointment;
  }
}
