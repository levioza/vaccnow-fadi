/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.*;
import eg.a1softech.vaccnow.domain.enumeration.PaymentMethod;
import eg.a1softech.vaccnow.repository.BranchVaccineAvailabilityRepository;
import eg.a1softech.vaccnow.repository.PatientRepository;
import eg.a1softech.vaccnow.repository.PatientVaccineAppointmentRepository;
import eg.a1softech.vaccnow.service.MailService;
import eg.a1softech.vaccnow.service.VaccinationService;
import eg.a1softech.vaccnow.service.dto.ConfirmAndPayVaccineDTO;
import eg.a1softech.vaccnow.service.dto.TakeVaccineDTO;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * VaccinationService implementation.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class VaccinationServiceImpl implements VaccinationService {

  private final Logger log = LoggerFactory.getLogger(
    VaccinationServiceImpl.class
  );

  private final PatientRepository patientRepository;

  private final PatientVaccineAppointmentRepository patientVaccineAppointmentRepository;

  private final BranchVaccineAvailabilityRepository branchVaccineAvailabilityRepository;

  private final MailService mailService;

  public VaccinationServiceImpl(
    PatientRepository patientRepository,
    PatientVaccineAppointmentRepository patientVaccineAppointmentRepository,
    BranchVaccineAvailabilityRepository branchVaccineAvailabilityRepository,
    MailService mailService
  ) {
    this.patientRepository = patientRepository;
    this.patientVaccineAppointmentRepository =
      patientVaccineAppointmentRepository;
    this.branchVaccineAvailabilityRepository =
      branchVaccineAvailabilityRepository;
    this.mailService = mailService;
  }

  @Override
  public Optional<PatientVaccineAppointment> confirmVaccinationAppointmentAndPay(
    ConfirmAndPayVaccineDTO confirmAndPayVaccineDTO
  ) {
    log.debug(
      "Request to confirm a vaccine appointment based on the provided dto : {}",
      confirmAndPayVaccineDTO
    );

    Optional<BranchVaccineAvailability> branchVaccineAvailability = branchVaccineAvailabilityRepository.findById(
      confirmAndPayVaccineDTO.getBvaId()
    );

    if (branchVaccineAvailability.isEmpty()) {
      throw new VaccinationException(
        "The branch vaccine availability id is not correct."
      );
    }

    Optional<Patient> patient = patientRepository.findById(
      confirmAndPayVaccineDTO.getPatientId()
    );

    if (patient.isEmpty()) {
      throw new VaccinationException("The patient id is not correct.");
    }

    if (
      confirmAndPayVaccineDTO.getPaymentMethod() == PaymentMethod.FAWRY &&
      confirmAndPayVaccineDTO.getFawryTransactionId().isEmpty()
    ) {
      throw new VaccinationException(
        "We can't consider this as a fawry payment without the fawry transaction id."
      );
    }

    if (
      confirmAndPayVaccineDTO.getPaymentMethod() == PaymentMethod.CREDIT &&
      confirmAndPayVaccineDTO.getCreditCardLast4Digits().isEmpty()
    ) {
      throw new VaccinationException(
        "We can't consider this as a credit card payment without the last 4 digits of the credit card."
      );
    }

    // Find the branch vaccine appointment based on these information.
    Optional<PatientVaccineAppointment> patientVaccineAppointmentCheck = patientVaccineAppointmentRepository.findOneByVaccinationDateTimeAndBranch_id_AndPatient_id_AndVaccine_id(
      branchVaccineAvailability.get().getVaccinationDateTime(),
      branchVaccineAvailability.get().getBranch().getId(),
      patient.get().getId(),
      branchVaccineAvailability.get().getVaccine().getId()
    );

    if (
      patientVaccineAppointmentCheck.isPresent() &&
      patientVaccineAppointmentCheck.get().isConfirmed().equals(true)
    ) {
      throw new VaccinationException(
        "You already reserved this vaccine appointment."
      );
    }

    Branch branch = branchVaccineAvailability.get().getBranch();
    Vaccine vaccine = branchVaccineAvailability.get().getVaccine();

    // Check for vaccine availability.
    Integer availableSeats = branchVaccineAvailability
      .get()
      .getAvailableSeats();
    if (availableSeats > 0) {
      branchVaccineAvailability.get().setAvailableSeats(availableSeats - 1);
      branchVaccineAvailabilityRepository.save(branchVaccineAvailability.get());
    } else {
      throw new VaccinationException(
        "The vaccine in this branch at the current time is no longer available."
      );
    }

    // Save a confirmation of the appointment.
    PatientVaccineAppointment patientVaccineAppointment = new PatientVaccineAppointment();
    patientVaccineAppointment.setPatient(patient.get()); // We made sure that the patient exists previously.
    patientVaccineAppointment.setBranch(branch);
    patientVaccineAppointment.setVaccine(vaccine);
    patientVaccineAppointment.setVaccinationDateTime(
      branchVaccineAvailability.get().getVaccinationDateTime()
    );
    patientVaccineAppointment.setAmountPaid(
      confirmAndPayVaccineDTO.getAmountPaid()
    );
    patientVaccineAppointment.setPaymentMethod(
      confirmAndPayVaccineDTO.getPaymentMethod()
    );

    if (confirmAndPayVaccineDTO.getPaymentMethod() == PaymentMethod.FAWRY) {
      patientVaccineAppointment.setFawryTransactionId(
        confirmAndPayVaccineDTO.getFawryTransactionId()
      );
    } else if (
      confirmAndPayVaccineDTO.getPaymentMethod() == PaymentMethod.CREDIT
    ) {
      patientVaccineAppointment.setCreditCardLast4Digits(
        confirmAndPayVaccineDTO.getCreditCardLast4Digits()
      );
    }

    patientVaccineAppointment.setConfirmed(true);
    PatientVaccineAppointment patientVaccineAppointmentToBeReturned = patientVaccineAppointmentRepository.save(
      patientVaccineAppointment
    );

    // Send confirmation email.
    mailService.sendPatientVaccineAppointmentConfirmation(
      patient.get(),
      patientVaccineAppointment
    );

    return Optional.of(patientVaccineAppointmentToBeReturned);
  }

  @Override
  public Optional<PatientVaccineAppointment> takeVaccine(
    TakeVaccineDTO takeVaccineDTO
  ) {
    log.debug(
      "Request to confirm that a vaccine appointment has been taken by the patient based on the provided dto : {}",
      takeVaccineDTO
    );

    // Get the patient.
    Optional<Patient> patient = patientRepository.findById(
      takeVaccineDTO.getPatientId()
    );

    if (patient.isEmpty()) {
      throw new VaccinationException("The patient id is not correct.");
    }

    // Get the branch vaccine availability.
    Optional<BranchVaccineAvailability> branchVaccineAvailability = branchVaccineAvailabilityRepository.findById(
      takeVaccineDTO.getBvaId()
    );

    if (branchVaccineAvailability.isEmpty()) {
      throw new VaccinationException(
        "The branch vaccine availability id is not correct."
      );
    }

    // Find the branch vaccine appointment based on these information.
    Optional<PatientVaccineAppointment> patientVaccineAppointment = patientVaccineAppointmentRepository.findOneByVaccinationDateTimeAndBranch_id_AndPatient_id_AndVaccine_id(
      branchVaccineAvailability.get().getVaccinationDateTime(),
      branchVaccineAvailability.get().getBranch().getId(),
      patient.get().getId(),
      branchVaccineAvailability.get().getVaccine().getId()
    );

    if (patientVaccineAppointment.isEmpty()) {
      throw new VaccinationException(
        "You are not eligible to take the vaccine. You should reserve your appointment prior before taking the vaccine."
      );
    } else {
      if (patientVaccineAppointment.get().isTaken() != null) {
        throw new VaccinationException(
          "You already took this vaccine at the specified appointment"
        );
      }

      patientVaccineAppointment.get().setTaken(true);
      PatientVaccineAppointment patientVaccineAppointmentToBeReturned = patientVaccineAppointmentRepository.save(
        patientVaccineAppointment.get()
      );

      // Send the certification email.
      mailService.sendThatThePatientVaccineHasBeenTakenConfirmation(
        patient.get(),
        patientVaccineAppointmentToBeReturned
      );

      return Optional.of(patientVaccineAppointmentToBeReturned);
    }
  }
}
