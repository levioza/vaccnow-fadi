/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.Patient;
import eg.a1softech.vaccnow.repository.PatientRepository;
import eg.a1softech.vaccnow.service.PatientService;
import eg.a1softech.vaccnow.service.dto.PatientDTO;
import eg.a1softech.vaccnow.service.mapper.PatientMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Patient}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class PatientServiceImpl implements PatientService {

  private final Logger log = LoggerFactory.getLogger(PatientServiceImpl.class);

  private final PatientRepository patientRepository;

  private final PatientMapper patientMapper;

  public PatientServiceImpl(
    PatientRepository patientRepository,
    PatientMapper patientMapper
  ) {
    this.patientRepository = patientRepository;
    this.patientMapper = patientMapper;
  }

  @Override
  public PatientDTO save(PatientDTO patientDTO) {
    log.debug("Request to save Patient : {}", patientDTO);
    Patient patient = patientMapper.toEntity(patientDTO);
    patient = patientRepository.save(patient);
    return patientMapper.toDto(patient);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<PatientDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Patients");
    return patientRepository.findAll(pageable).map(patientMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<PatientDTO> findOne(Long id) {
    log.debug("Request to get Patient : {}", id);
    return patientRepository.findById(id).map(patientMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Patient : {}", id);
    patientRepository.deleteById(id);
  }
}
