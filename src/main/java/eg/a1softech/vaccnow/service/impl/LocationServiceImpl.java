/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.Location;
import eg.a1softech.vaccnow.repository.LocationRepository;
import eg.a1softech.vaccnow.service.LocationService;
import eg.a1softech.vaccnow.service.dto.LocationDTO;
import eg.a1softech.vaccnow.service.mapper.LocationMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Location}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class LocationServiceImpl implements LocationService {

  private final Logger log = LoggerFactory.getLogger(LocationServiceImpl.class);

  private final LocationRepository locationRepository;

  private final LocationMapper locationMapper;

  public LocationServiceImpl(
    LocationRepository locationRepository,
    LocationMapper locationMapper
  ) {
    this.locationRepository = locationRepository;
    this.locationMapper = locationMapper;
  }

  @Override
  public LocationDTO save(LocationDTO locationDTO) {
    log.debug("Request to save Location : {}", locationDTO);
    Location location = locationMapper.toEntity(locationDTO);
    location = locationRepository.save(location);
    return locationMapper.toDto(location);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<LocationDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Locations");
    return locationRepository.findAll(pageable).map(locationMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<LocationDTO> findOne(Long id) {
    log.debug("Request to get Location : {}", id);
    return locationRepository.findById(id).map(locationMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Location : {}", id);
    locationRepository.deleteById(id);
  }
}
