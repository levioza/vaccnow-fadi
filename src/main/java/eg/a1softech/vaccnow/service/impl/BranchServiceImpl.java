/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.Branch;
import eg.a1softech.vaccnow.repository.BranchRepository;
import eg.a1softech.vaccnow.service.BranchService;
import eg.a1softech.vaccnow.service.dto.BranchDTO;
import eg.a1softech.vaccnow.service.mapper.BranchMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Branch}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class BranchServiceImpl implements BranchService {

  private final Logger log = LoggerFactory.getLogger(BranchServiceImpl.class);

  private final BranchRepository branchRepository;

  private final BranchMapper branchMapper;

  public BranchServiceImpl(
    BranchRepository branchRepository,
    BranchMapper branchMapper
  ) {
    this.branchRepository = branchRepository;
    this.branchMapper = branchMapper;
  }

  @Override
  public BranchDTO save(BranchDTO branchDTO) {
    log.debug("Request to save Branch : {}", branchDTO);
    Branch branch = branchMapper.toEntity(branchDTO);
    branch = branchRepository.save(branch);
    return branchMapper.toDto(branch);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<BranchDTO> findAll(Pageable pageable) {
    log.debug("Request to get all Branches");
    return branchRepository.findAll(pageable).map(branchMapper::toDto);
  }

  public Page<BranchDTO> findAllWithEagerRelationships(Pageable pageable) {
    return branchRepository
      .findAllWithEagerRelationships(pageable)
      .map(branchMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<BranchDTO> findOne(Long id) {
    log.debug("Request to get Branch : {}", id);
    return branchRepository
      .findOneWithEagerRelationships(id)
      .map(branchMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete Branch : {}", id);
    branchRepository.deleteById(id);
  }
}
