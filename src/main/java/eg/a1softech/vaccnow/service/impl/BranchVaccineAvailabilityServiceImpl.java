/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.BranchVaccineAvailability;
import eg.a1softech.vaccnow.repository.BranchVaccineAvailabilityRepository;
import eg.a1softech.vaccnow.service.BranchVaccineAvailabilityService;
import eg.a1softech.vaccnow.service.dto.BranchVaccineAvailabilityDTO;
import eg.a1softech.vaccnow.service.mapper.BranchVaccineAvailabilityMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BranchVaccineAvailability}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class BranchVaccineAvailabilityServiceImpl
  implements BranchVaccineAvailabilityService {

  private final Logger log = LoggerFactory.getLogger(
    BranchVaccineAvailabilityServiceImpl.class
  );

  private final BranchVaccineAvailabilityRepository branchVaccineAvailabilityRepository;

  private final BranchVaccineAvailabilityMapper branchVaccineAvailabilityMapper;

  public BranchVaccineAvailabilityServiceImpl(
    BranchVaccineAvailabilityRepository branchVaccineAvailabilityRepository,
    BranchVaccineAvailabilityMapper branchVaccineAvailabilityMapper
  ) {
    this.branchVaccineAvailabilityRepository =
      branchVaccineAvailabilityRepository;
    this.branchVaccineAvailabilityMapper = branchVaccineAvailabilityMapper;
  }

  @Override
  public BranchVaccineAvailabilityDTO save(
    BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO
  ) {
    log.debug(
      "Request to save BranchVaccineAvailability : {}",
      branchVaccineAvailabilityDTO
    );
    BranchVaccineAvailability branchVaccineAvailability = branchVaccineAvailabilityMapper.toEntity(
      branchVaccineAvailabilityDTO
    );
    branchVaccineAvailability =
      branchVaccineAvailabilityRepository.save(branchVaccineAvailability);
    return branchVaccineAvailabilityMapper.toDto(branchVaccineAvailability);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<BranchVaccineAvailabilityDTO> findAll(Pageable pageable) {
    log.debug("Request to get all BranchVaccineAvailabilities");
    return branchVaccineAvailabilityRepository
      .findAll(pageable)
      .map(branchVaccineAvailabilityMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<BranchVaccineAvailabilityDTO> findAllByBranchId(
    Pageable pageable,
    Long branchId
  ) {
    log.debug("Request to get all BranchVaccineAvailabilities by branch id");
    return branchVaccineAvailabilityRepository
      .findAllByBranch_id(branchId, pageable)
      .map(branchVaccineAvailabilityMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<BranchVaccineAvailabilityDTO> findByIdAndByBranchId(
    Long id,
    Long branchId
  ) {
    return branchVaccineAvailabilityRepository
      .findByIdAndBranch_id(id, branchId)
      .map(branchVaccineAvailabilityMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<BranchVaccineAvailabilityDTO> findOne(Long id) {
    log.debug("Request to get BranchVaccineAvailability : {}", id);
    return branchVaccineAvailabilityRepository
      .findById(id)
      .map(branchVaccineAvailabilityMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete BranchVaccineAvailability : {}", id);
    branchVaccineAvailabilityRepository.deleteById(id);
  }
}
