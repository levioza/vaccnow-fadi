/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.service.impl;

import eg.a1softech.vaccnow.domain.PatientVaccineAppointment;
import eg.a1softech.vaccnow.repository.PatientVaccineAppointmentRepository;
import eg.a1softech.vaccnow.service.PatientVaccineAppointmentService;
import eg.a1softech.vaccnow.service.dto.PatientVaccineAppointmentDTO;
import eg.a1softech.vaccnow.service.mapper.PatientVaccineAppointmentMapper;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PatientVaccineAppointment}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Service
@Transactional
public class PatientVaccineAppointmentServiceImpl
  implements PatientVaccineAppointmentService {

  private final Logger log = LoggerFactory.getLogger(
    PatientVaccineAppointmentServiceImpl.class
  );

  private final PatientVaccineAppointmentRepository patientVaccineAppointmentRepository;

  private final PatientVaccineAppointmentMapper patientVaccineAppointmentMapper;

  public PatientVaccineAppointmentServiceImpl(
    PatientVaccineAppointmentRepository patientVaccineAppointmentRepository,
    PatientVaccineAppointmentMapper patientVaccineAppointmentMapper
  ) {
    this.patientVaccineAppointmentRepository =
      patientVaccineAppointmentRepository;
    this.patientVaccineAppointmentMapper = patientVaccineAppointmentMapper;
  }

  @Override
  public PatientVaccineAppointmentDTO save(
    PatientVaccineAppointmentDTO patientVaccineAppointmentDTO
  ) {
    log.debug(
      "Request to save PatientVaccineAppointment : {}",
      patientVaccineAppointmentDTO
    );
    PatientVaccineAppointment patientVaccineAppointment = patientVaccineAppointmentMapper.toEntity(
      patientVaccineAppointmentDTO
    );
    patientVaccineAppointment =
      patientVaccineAppointmentRepository.save(patientVaccineAppointment);
    return patientVaccineAppointmentMapper.toDto(patientVaccineAppointment);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<PatientVaccineAppointmentDTO> findAll(Pageable pageable) {
    log.debug("Request to get all PatientVaccineAppointments");
    return patientVaccineAppointmentRepository
      .findAll(pageable)
      .map(patientVaccineAppointmentMapper::toDto);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<PatientVaccineAppointmentDTO> findOne(Long id) {
    log.debug("Request to get PatientVaccineAppointment : {}", id);
    return patientVaccineAppointmentRepository
      .findById(id)
      .map(patientVaccineAppointmentMapper::toDto);
  }

  @Override
  public void delete(Long id) {
    log.debug("Request to delete PatientVaccineAppointment : {}", id);
    patientVaccineAppointmentRepository.deleteById(id);
  }

  @Override
  public Page<PatientVaccineAppointmentDTO> findAllByBranchIdAndTakenTrue(
    Long branchId,
    Pageable pageable
  ) {
    log.debug(
      "Request a page of the PatientVaccineAppointment taken at the branch id : {}",
      branchId
    );
    return patientVaccineAppointmentRepository
      .findAllByBranch_idAndTakenTrue(branchId, pageable)
      .map(patientVaccineAppointmentMapper::toDto);
  }

  @Override
  public Page<PatientVaccineAppointmentDTO> findAllByVaccinationDateTimeBetweenAndTakenTrue(
    ZonedDateTime vaccinationDateTimeFrom,
    ZonedDateTime vaccinationDateTimeTo,
    Pageable pageable
  ) {
    log.debug(
      "Request a page of the PatientVaccineAppointment taken between : {} and {}",
      vaccinationDateTimeFrom,
      vaccinationDateTimeTo
    );
    return patientVaccineAppointmentRepository
      .findAllByVaccinationDateTimeBetweenAndTakenTrue(
        vaccinationDateTimeFrom,
        vaccinationDateTimeTo,
        pageable
      )
      .map(patientVaccineAppointmentMapper::toDto);
  }

  @Override
  public Page<PatientVaccineAppointmentDTO> findAllByVaccinationDateTimeBetweenAndConfirmedTrueAndTakenFalse(
    ZonedDateTime vaccinationDateTimeFrom,
    ZonedDateTime vaccinationDateTimeTo,
    Pageable pageable
  ) {
    log.debug(
      "Request a page of the PatientVaccineAppointment taken between : {} and {}",
      vaccinationDateTimeFrom,
      vaccinationDateTimeTo
    );
    return patientVaccineAppointmentRepository
      .findAllByVaccinationDateTimeBetweenAndConfirmedTrueAndTakenFalse(
        vaccinationDateTimeFrom,
        vaccinationDateTimeTo,
        pageable
      )
      .map(patientVaccineAppointmentMapper::toDto);
  }
}
