/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.BranchService;
import eg.a1softech.vaccnow.service.BranchVaccineAvailabilityService;
import eg.a1softech.vaccnow.service.dto.BranchDTO;
import eg.a1softech.vaccnow.service.dto.BranchVaccineAvailabilityDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * The AvailabilityResource as per the instructed task documentation
 *
 * @author Fadi William Ghali Abdelmessih
 */
@RestController
@RequestMapping("/api/availability")
@Tag(name = "AvailabilityResource")
public class AvailabilityResource {

  private final Logger log = LoggerFactory.getLogger(
    AvailabilityResource.class
  );

  private final BranchService branchService;

  private final BranchVaccineAvailabilityService branchVaccineAvailabilityService;

  public AvailabilityResource(
    BranchService branchService,
    BranchVaccineAvailabilityService branchVaccineAvailabilityService
  ) {
    this.branchService = branchService;
    this.branchVaccineAvailabilityService = branchVaccineAvailabilityService;
  }

  /**
   * {@code GET  /branches} : get all the branches.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of branches in body.
   */
  @GetMapping("/branches")
  public ResponseEntity<List<BranchDTO>> getAllBranches(Pageable pageable) {
    log.debug("REST request to get a page of Branches");
    Page<BranchDTO> page = branchService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /branchVaccines/:branchId} : get the vaccines available in a specific branch.
   *
   * @param branchId the branch id.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the branchDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/branchVaccines/{branchId}")
  public ResponseEntity<BranchDTO> getBranchVaccinesByBranchId(
    @PathVariable Long branchId
  ) {
    log.debug("REST request to get Branch's Vaccines : {}", branchId);
    Optional<BranchDTO> branchDTO = branchService.findOne(branchId);
    return ResponseUtil.wrapOrNotFound(branchDTO);
  }

  /**
   * {@code GET  /branchVaccinesAvailabilities/{branchId}} : get all the vaccines time availabilities in a specific branch.
   *
   * @param pageable the pagination information.
   * @param branchId the branch id.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of branches in body.
   */
  @GetMapping("/branchVaccinesAvailabilities/{branchId}")
  public ResponseEntity<List<BranchVaccineAvailabilityDTO>> getAllBranchesVaccinesAvailabilitiesByBranchId(
    Pageable pageable,
    @PathVariable Long branchId
  ) {
    log.debug(
      "REST request to get a page of Branch Vaccine Availabilities by branch id"
    );
    Page<BranchVaccineAvailabilityDTO> page = branchVaccineAvailabilityService.findAllByBranchId(
      pageable,
      branchId
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /branchVaccineAvailability/{branchId}/{bvaId}} : get a specific vaccine time availability in a specific branch.
   *
   * @param bvaId the branch vaccine availability id.
   * @param branchId the branch id.
   *
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of branches in body.
   */
  @GetMapping("/branchVaccineAvailability/{branchId}/{bvaId}")
  public ResponseEntity<BranchVaccineAvailabilityDTO> getAllBranchesVaccinesAvailabilitiesByBranchId(
    @PathVariable Long branchId,
    @PathVariable Long bvaId
  ) {
    log.debug(
      "REST request to get a specific branch vaccine availability by its id and the branch id"
    );
    Optional<BranchVaccineAvailabilityDTO> branchVaccineAvailabilityDTO = branchVaccineAvailabilityService.findByIdAndByBranchId(
      branchId,
      bvaId
    );
    return ResponseUtil.wrapOrNotFound(branchVaccineAvailabilityDTO);
  }
}
