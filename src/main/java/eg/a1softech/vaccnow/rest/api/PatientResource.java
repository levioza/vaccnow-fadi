/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.HeaderUtil;
import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.PatientService;
import eg.a1softech.vaccnow.service.dto.PatientDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link eg.a1softech.vaccnow.domain.Patient}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "PatientResource")
public class PatientResource {

  private final Logger log = LoggerFactory.getLogger(PatientResource.class);

  private static final String ENTITY_NAME = "patient";

  @Value("${spring.application.name}")
  private String applicationName;

  private final PatientService patientService;

  public PatientResource(PatientService patientService) {
    this.patientService = patientService;
  }

  /**
   * {@code POST  /patients} : Create a new patient.
   *
   * @param patientDTO the patientDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new patientDTO, or with status {@code 400 (Bad Request)} if the patient has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/patients")
  public ResponseEntity<PatientDTO> createPatient(
    @Valid @RequestBody PatientDTO patientDTO
  ) throws URISyntaxException {
    log.debug("REST request to save Patient : {}", patientDTO);
    if (patientDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "A new patient cannot already have an ID")
        .body(null);
    }
    PatientDTO result = patientService.save(patientDTO);
    return ResponseEntity
      .created(new URI("/api/patients/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /patients} : Updates an existing patient.
   *
   * @param patientDTO the patientDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patientDTO,
   * or with status {@code 400 (Bad Request)} if the patientDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the patientDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/patients")
  public ResponseEntity<PatientDTO> updatePatient(
    @Valid @RequestBody PatientDTO patientDTO
  ) throws URISyntaxException {
    log.debug("REST request to update Patient : {}", patientDTO);
    if (patientDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The patient id cannot be null")
        .body(null);
    }
    PatientDTO result = patientService.save(patientDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          patientDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /patients} : get all the patients.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of patients in body.
   */
  @GetMapping("/patients")
  public ResponseEntity<List<PatientDTO>> getAllPatients(Pageable pageable) {
    log.debug("REST request to get a page of Patients");
    Page<PatientDTO> page = patientService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /patients/:id} : get the "id" patient.
   *
   * @param id the id of the patientDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the patientDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/patients/{id}")
  public ResponseEntity<PatientDTO> getPatient(@PathVariable Long id) {
    log.debug("REST request to get Patient : {}", id);
    Optional<PatientDTO> patientDTO = patientService.findOne(id);
    return ResponseUtil.wrapOrNotFound(patientDTO);
  }

  /**
   * {@code DELETE  /patients/:id} : delete the "id" patient.
   *
   * @param id the id of the patientDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/patients/{id}")
  public ResponseEntity<Void> deletePatient(@PathVariable Long id) {
    log.debug("REST request to delete Patient : {}", id);
    patientService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
