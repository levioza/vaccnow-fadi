/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.HeaderUtil;
import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.BranchService;
import eg.a1softech.vaccnow.service.dto.BranchDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link eg.a1softech.vaccnow.domain.Branch}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "BranchResource")
public class BranchResource {

  private final Logger log = LoggerFactory.getLogger(BranchResource.class);

  private static final String ENTITY_NAME = "branch";

  @Value("${spring.application.name}")
  private String applicationName;

  private final BranchService branchService;

  public BranchResource(BranchService branchService) {
    this.branchService = branchService;
  }

  /**
   * {@code POST  /branches} : Create a new branch.
   *
   * @param branchDTO the branchDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new branchDTO, or with status {@code 400 (Bad Request)} if the branch has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/branches")
  public ResponseEntity<BranchDTO> createBranch(
    @Valid @RequestBody BranchDTO branchDTO
  ) throws URISyntaxException {
    log.debug("REST request to save Branch : {}", branchDTO);
    if (branchDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "A new branch cannot already have an ID")
        .body(null);
    }
    BranchDTO result = branchService.save(branchDTO);
    return ResponseEntity
      .created(new URI("/api/branches/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /branches} : Updates an existing branch.
   *
   * @param branchDTO the branchDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated branchDTO,
   * or with status {@code 400 (Bad Request)} if the branchDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the branchDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/branches")
  public ResponseEntity<BranchDTO> updateBranch(
    @Valid @RequestBody BranchDTO branchDTO
  ) throws URISyntaxException {
    log.debug("REST request to update Branch : {}", branchDTO);
    if (branchDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The branch id cannot be null")
        .body(null);
    }
    BranchDTO result = branchService.save(branchDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          branchDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /branches} : get all the branches.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of branches in body.
   */
  @GetMapping("/branches")
  public ResponseEntity<List<BranchDTO>> getAllBranches(Pageable pageable) {
    log.debug("REST request to get a page of Branches");
    Page<BranchDTO> page = branchService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /branches/:id} : get the "id" branch.
   *
   * @param id the id of the branchDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the branchDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/branches/{id}")
  public ResponseEntity<BranchDTO> getBranch(@PathVariable Long id) {
    log.debug("REST request to get Branch : {}", id);
    Optional<BranchDTO> branchDTO = branchService.findOne(id);
    return ResponseUtil.wrapOrNotFound(branchDTO);
  }

  /**
   * {@code DELETE  /branches/:id} : delete the "id" branch.
   *
   * @param id the id of the branchDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/branches/{id}")
  public ResponseEntity<Void> deleteBranch(@PathVariable Long id) {
    log.debug("REST request to delete Branch : {}", id);
    branchService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
