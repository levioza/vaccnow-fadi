/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.service.PatientVaccineAppointmentService;
import eg.a1softech.vaccnow.service.dto.PatientVaccineAppointmentDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/reporting")
@Tag(name = "ReportingResource")
public class ReportingResource {

  private final Logger log = LoggerFactory.getLogger(ReportingResource.class);

  private final PatientVaccineAppointmentService patientVaccineAppointmentService;

  public ReportingResource(
    PatientVaccineAppointmentService patientVaccineAppointmentService
  ) {
    this.patientVaccineAppointmentService = patientVaccineAppointmentService;
  }

  /**
   * {@code GET  /takenVaccinesAtBranch/:branchId} : get the taken vaccinations at a specific branch.
   *
   * @param branchId the branch id.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body of the taken vaccinations that occurred at the specified branch or with status {@code 400 (Bad Request)} if any of the validation/business logic errors occurred.
   */
  @GetMapping("/takenVaccinationsAtBranch/{branchId}")
  public ResponseEntity<List<PatientVaccineAppointmentDTO>> getTakenVaccinationsAtBranch(
    Pageable pageable,
    @PathVariable Long branchId
  ) {
    log.debug(
      "REST request to get the vaccines taken at branch id : {}",
      branchId
    );
    Page<PatientVaccineAppointmentDTO> page = patientVaccineAppointmentService.findAllByBranchIdAndTakenTrue(
      branchId,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /takenVaccinesAt/:vaccinationDateTime} : get the taken vaccinations at a specific date.
   *
   * @param vaccinationDateTime the vaccination date time.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body of the taken vaccinations that occurred at the specified date or with status {@code 400 (Bad Request)} if any of the validation/business logic errors occurred.
   */
  @GetMapping("/takenVaccinationsAt/{vaccinationDateTime}")
  public ResponseEntity<List<PatientVaccineAppointmentDTO>> getTakenVaccinationsAt(
    Pageable pageable,
    @PathVariable @DateTimeFormat(
      iso = DateTimeFormat.ISO.DATE_TIME
    ) ZonedDateTime vaccinationDateTime
  ) {
    ZonedDateTime dateTimeFrom = vaccinationDateTime;
    dateTimeFrom = dateTimeFrom.with(LocalTime.of(0, 0));
    ZonedDateTime dateTimeTo = vaccinationDateTime;
    dateTimeTo = dateTimeTo.with(LocalTime.of(23, 59));
    log.debug(
      "REST request to get the vaccines taken at date : {}",
      Date.from(vaccinationDateTime.toInstant())
    );
    Page<PatientVaccineAppointmentDTO> page = patientVaccineAppointmentService.findAllByVaccinationDateTimeBetweenAndTakenTrue(
      dateTimeFrom,
      dateTimeTo,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /confirmedVaccinationsBetween/:vaccinationDateTimeFrom/:vaccinationDateTimeTo} : get the confirmed vaccinations between the specified date times.
   *
   * @param vaccinationDateTimeFrom the vaccination date time from.
   * @param vaccinationDateTimeTo the vaccination date time from.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body of the confirmed vaccinations at the specified date or with status {@code 400 (Bad Request)} if any of the validation/business logic errors occurred.
   */
  @GetMapping(
    "/confirmedVaccinationsBetween/{vaccinationDateTimeFrom}/{vaccinationDateTimeTo}"
  )
  public ResponseEntity<List<PatientVaccineAppointmentDTO>> getConfirmedVaccinationsBetween(
    Pageable pageable,
    @PathVariable("vaccinationDateTimeFrom") @DateTimeFormat(
      iso = DateTimeFormat.ISO.DATE_TIME
    ) ZonedDateTime vaccinationDateTimeFrom,
    @PathVariable("vaccinationDateTimeTo") @DateTimeFormat(
      iso = DateTimeFormat.ISO.DATE_TIME
    ) ZonedDateTime vaccinationDateTimeTo
  ) {
    log.debug(
      "REST request to get the vaccines taken between {} and {}",
      vaccinationDateTimeFrom,
      vaccinationDateTimeTo
    );
    Page<PatientVaccineAppointmentDTO> page = patientVaccineAppointmentService.findAllByVaccinationDateTimeBetweenAndConfirmedTrueAndTakenFalse(
      vaccinationDateTimeFrom,
      vaccinationDateTimeTo,
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }
}
