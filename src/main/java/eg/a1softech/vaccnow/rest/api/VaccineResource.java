/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.HeaderUtil;
import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.VaccineService;
import eg.a1softech.vaccnow.service.dto.VaccineDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link eg.a1softech.vaccnow.domain.Vaccine}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "VaccineResource")
public class VaccineResource {

  private final Logger log = LoggerFactory.getLogger(VaccineResource.class);

  private static final String ENTITY_NAME = "vaccine";

  @Value("${spring.application.name}")
  private String applicationName;

  private final VaccineService vaccineService;

  public VaccineResource(VaccineService vaccineService) {
    this.vaccineService = vaccineService;
  }

  /**
   * {@code POST  /vaccines} : Create a new vaccine.
   *
   * @param vaccineDTO the vaccineDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vaccineDTO, or with status {@code 400 (Bad Request)} if the vaccine has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/vaccines")
  public ResponseEntity<VaccineDTO> createVaccine(
    @Valid @RequestBody VaccineDTO vaccineDTO
  ) throws URISyntaxException {
    log.debug("REST request to save Vaccine : {}", vaccineDTO);
    if (vaccineDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "A new vaccine cannot already have an ID")
        .body(null);
    }
    VaccineDTO result = vaccineService.save(vaccineDTO);
    return ResponseEntity
      .created(new URI("/api/vaccines/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /vaccines} : Updates an existing vaccine.
   *
   * @param vaccineDTO the vaccineDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vaccineDTO,
   * or with status {@code 400 (Bad Request)} if the vaccineDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the vaccineDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/vaccines")
  public ResponseEntity<VaccineDTO> updateVaccine(
    @Valid @RequestBody VaccineDTO vaccineDTO
  ) throws URISyntaxException {
    log.debug("REST request to update Vaccine : {}", vaccineDTO);
    if (vaccineDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The vaccine id cannot be null")
        .body(null);
    }
    VaccineDTO result = vaccineService.save(vaccineDTO);
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          vaccineDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /vaccines} : get all the vaccines.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vaccines in body.
   */
  @GetMapping("/vaccines")
  public ResponseEntity<List<VaccineDTO>> getAllVaccines(Pageable pageable) {
    log.debug("REST request to get a page of Vaccines");
    Page<VaccineDTO> page = vaccineService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /vaccines/:id} : get the "id" vaccine.
   *
   * @param id the id of the vaccineDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vaccineDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/vaccines/{id}")
  public ResponseEntity<VaccineDTO> getVaccine(@PathVariable Long id) {
    log.debug("REST request to get Vaccine : {}", id);
    Optional<VaccineDTO> vaccineDTO = vaccineService.findOne(id);
    return ResponseUtil.wrapOrNotFound(vaccineDTO);
  }

  /**
   * {@code DELETE  /vaccines/:id} : delete the "id" vaccine.
   *
   * @param id the id of the vaccineDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/vaccines/{id}")
  public ResponseEntity<Void> deleteVaccine(@PathVariable Long id) {
    log.debug("REST request to delete Vaccine : {}", id);
    vaccineService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
