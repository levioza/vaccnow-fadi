/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.domain.PatientVaccineAppointment;
import eg.a1softech.vaccnow.service.VaccinationService;
import eg.a1softech.vaccnow.service.dto.ConfirmAndPayVaccineDTO;
import eg.a1softech.vaccnow.service.dto.TakeVaccineDTO;
import eg.a1softech.vaccnow.service.impl.VaccinationException;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The VaccinationResource as per the instructed task documentation.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api/vaccination")
@Tag(name = "VaccinationResource")
public class VaccinationResource {

  private final Logger log = LoggerFactory.getLogger(VaccinationResource.class);

  private final VaccinationService vaccinationService;

  public VaccinationResource(VaccinationService vaccinationService) {
    this.vaccinationService = vaccinationService;
  }

  /**
   * {@code POST  /confirmVaccinationAppointmentAndPay} : Confirm a vaccination appointment.
   *
   * @param confirmAndPayVaccineDTO the confirm and pay vaccine dto that represents the patient who will confirm and pay for a vaccine appointment.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body of the confirmed patient vaccination appointment or with status {@code 400 (Bad Request)} if any of the validation/business logic errors occurred.
   * @throws VaccinationException if any of the validation/business logic errors occurred.
   */
  @PostMapping("/confirmVaccinationAppointmentAndPay")
  public ResponseEntity<PatientVaccineAppointment> confirmVaccinationAppointmentAndPay(
    @RequestBody ConfirmAndPayVaccineDTO confirmAndPayVaccineDTO
  ) {
    log.debug(
      "REST request to confirm a patient vaccine appointment : {}",
      confirmAndPayVaccineDTO
    );

    Optional<PatientVaccineAppointment> patientVaccineAppointment = vaccinationService.confirmVaccinationAppointmentAndPay(
      confirmAndPayVaccineDTO
    );

    return patientVaccineAppointment
      .map(ResponseEntity::ok)
      .orElseGet(() -> ResponseEntity.badRequest().build());
  }

  /**
   * {@code POST  /takeVaccine} : Specify that a vaccine has been taken by a patient at the specific appointment.
   *
   * @param takeVaccineDTO the take vaccine dto that represents that a vaccine has been taken by a patient at the specific appointment.
   *
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body of the taken patient vaccination appointment or with status {@code 400 (Bad Request)} if any of the validation/business logic errors occurred.
   * @throws VaccinationException if any of the validation/business logic errors occurred.
   */
  @PostMapping("/takeVaccine")
  public ResponseEntity<PatientVaccineAppointment> takeVaccine(
    @RequestBody TakeVaccineDTO takeVaccineDTO
  ) {
    log.debug(
      "REST request to confirm that a vaccine has been taken by a patient at the specific appointment : {}",
      takeVaccineDTO
    );

    Optional<PatientVaccineAppointment> patientVaccineAppointment = vaccinationService.takeVaccine(
      takeVaccineDTO
    );

    return patientVaccineAppointment
      .map(ResponseEntity::ok)
      .orElseGet(() -> ResponseEntity.badRequest().build());
  }
}
