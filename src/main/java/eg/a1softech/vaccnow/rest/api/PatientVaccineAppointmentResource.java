/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.HeaderUtil;
import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.PatientVaccineAppointmentService;
import eg.a1softech.vaccnow.service.dto.PatientVaccineAppointmentDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link eg.a1softech.vaccnow.domain.PatientVaccineAppointment}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "PatientVaccineAppointmentResource")
public class PatientVaccineAppointmentResource {

  private final Logger log = LoggerFactory.getLogger(
    PatientVaccineAppointmentResource.class
  );

  private static final String ENTITY_NAME = "patientVaccineAppointment";

  @Value("${spring.application.name}")
  private String applicationName;

  private final PatientVaccineAppointmentService patientVaccineAppointmentService;

  public PatientVaccineAppointmentResource(
    PatientVaccineAppointmentService patientVaccineAppointmentService
  ) {
    this.patientVaccineAppointmentService = patientVaccineAppointmentService;
  }

  /**
   * {@code POST  /patient-vaccine-appointments} : Create a new patientVaccineAppointment.
   *
   * @param patientVaccineAppointmentDTO the patientVaccineAppointmentDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new patientVaccineAppointmentDTO, or with status {@code 400 (Bad Request)} if the patientVaccineAppointment has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/patient-vaccine-appointments")
  public ResponseEntity<PatientVaccineAppointmentDTO> createPatientVaccineAppointment(
    @Valid @RequestBody PatientVaccineAppointmentDTO patientVaccineAppointmentDTO
  ) throws URISyntaxException {
    log.debug(
      "REST request to save PatientVaccineAppointment : {}",
      patientVaccineAppointmentDTO
    );
    if (patientVaccineAppointmentDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "A new patientVaccineAppointment cannot already have an ID"
        )
        .body(null);
    }
    PatientVaccineAppointmentDTO result = patientVaccineAppointmentService.save(
      patientVaccineAppointmentDTO
    );
    return ResponseEntity
      .created(new URI("/api/patient-vaccine-appointments/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /patient-vaccine-appointments} : Updates an existing patientVaccineAppointment.
   *
   * @param patientVaccineAppointmentDTO the patientVaccineAppointmentDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patientVaccineAppointmentDTO,
   * or with status {@code 400 (Bad Request)} if the patientVaccineAppointmentDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the patientVaccineAppointmentDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/patient-vaccine-appointments")
  public ResponseEntity<PatientVaccineAppointmentDTO> updatePatientVaccineAppointment(
    @Valid @RequestBody PatientVaccineAppointmentDTO patientVaccineAppointmentDTO
  ) throws URISyntaxException {
    log.debug(
      "REST request to update PatientVaccineAppointment : {}",
      patientVaccineAppointmentDTO
    );
    if (patientVaccineAppointmentDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The patientVaccineAppointment id cannot be null")
        .body(null);
    }
    PatientVaccineAppointmentDTO result = patientVaccineAppointmentService.save(
      patientVaccineAppointmentDTO
    );
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          patientVaccineAppointmentDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /patient-vaccine-appointments} : get all the patientVaccineAppointments.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of patientVaccineAppointments in body.
   */
  @GetMapping("/patient-vaccine-appointments")
  public ResponseEntity<List<PatientVaccineAppointmentDTO>> getAllPatientVaccineAppointments(
    Pageable pageable
  ) {
    log.debug("REST request to get a page of PatientVaccineAppointments");
    Page<PatientVaccineAppointmentDTO> page = patientVaccineAppointmentService.findAll(
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /patient-vaccine-appointments/:id} : get the "id" patientVaccineAppointment.
   *
   * @param id the id of the patientVaccineAppointmentDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the patientVaccineAppointmentDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/patient-vaccine-appointments/{id}")
  public ResponseEntity<PatientVaccineAppointmentDTO> getPatientVaccineAppointment(
    @PathVariable Long id
  ) {
    log.debug("REST request to get PatientVaccineAppointment : {}", id);
    Optional<PatientVaccineAppointmentDTO> patientVaccineAppointmentDTO = patientVaccineAppointmentService.findOne(
      id
    );
    return ResponseUtil.wrapOrNotFound(patientVaccineAppointmentDTO);
  }

  /**
   * {@code DELETE  /patient-vaccine-appointments/:id} : delete the "id" patientVaccineAppointment.
   *
   * @param id the id of the patientVaccineAppointmentDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/patient-vaccine-appointments/{id}")
  public ResponseEntity<Void> deletePatientVaccineAppointment(
    @PathVariable Long id
  ) {
    log.debug("REST request to delete PatientVaccineAppointment : {}", id);
    patientVaccineAppointmentService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
