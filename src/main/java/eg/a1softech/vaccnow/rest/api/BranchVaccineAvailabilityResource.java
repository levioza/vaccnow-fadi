/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.api;

import eg.a1softech.vaccnow.rest.util.HeaderUtil;
import eg.a1softech.vaccnow.rest.util.PaginationUtil;
import eg.a1softech.vaccnow.rest.util.ResponseUtil;
import eg.a1softech.vaccnow.service.BranchVaccineAvailabilityService;
import eg.a1softech.vaccnow.service.dto.BranchVaccineAvailabilityDTO;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link eg.a1softech.vaccnow.domain.BranchVaccineAvailability}.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "BranchVaccineAvailabilityResource")
public class BranchVaccineAvailabilityResource {

  private final Logger log = LoggerFactory.getLogger(
    BranchVaccineAvailabilityResource.class
  );

  private static final String ENTITY_NAME = "branchVaccineAvailability";

  @Value("${spring.application.name}")
  private String applicationName;

  private final BranchVaccineAvailabilityService branchVaccineAvailabilityService;

  public BranchVaccineAvailabilityResource(
    BranchVaccineAvailabilityService branchVaccineAvailabilityService
  ) {
    this.branchVaccineAvailabilityService = branchVaccineAvailabilityService;
  }

  /**
   * {@code POST  /branch-vaccine-availabilities} : Create a new branchVaccineAvailability.
   *
   * @param branchVaccineAvailabilityDTO the branchVaccineAvailabilityDTO to create.
   * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new branchVaccineAvailabilityDTO, or with status {@code 400 (Bad Request)} if the branchVaccineAvailability has already an ID.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PostMapping("/branch-vaccine-availabilities")
  public ResponseEntity<BranchVaccineAvailabilityDTO> createBranchVaccineAvailability(
    @Valid @RequestBody BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO
  ) throws URISyntaxException {
    log.debug(
      "REST request to save BranchVaccineAvailability : {}",
      branchVaccineAvailabilityDTO
    );
    if (branchVaccineAvailabilityDTO.getId() != null) {
      return ResponseEntity
        .badRequest()
        .header(
          "Failure",
          "A new branchVaccineAvailability cannot already have an ID"
        )
        .body(null);
    }
    BranchVaccineAvailabilityDTO result = branchVaccineAvailabilityService.save(
      branchVaccineAvailabilityDTO
    );
    return ResponseEntity
      .created(new URI("/api/branch-vaccine-availabilities/" + result.getId()))
      .headers(
        HeaderUtil.createEntityCreationAlert(
          applicationName,
          false,
          ENTITY_NAME,
          result.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code PUT  /branch-vaccine-availabilities} : Updates an existing branchVaccineAvailability.
   *
   * @param branchVaccineAvailabilityDTO the branchVaccineAvailabilityDTO to update.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated branchVaccineAvailabilityDTO,
   * or with status {@code 400 (Bad Request)} if the branchVaccineAvailabilityDTO is not valid,
   * or with status {@code 500 (Internal Server Error)} if the branchVaccineAvailabilityDTO couldn't be updated.
   * @throws URISyntaxException if the Location URI syntax is incorrect.
   */
  @PutMapping("/branch-vaccine-availabilities")
  public ResponseEntity<BranchVaccineAvailabilityDTO> updateBranchVaccineAvailability(
    @Valid @RequestBody BranchVaccineAvailabilityDTO branchVaccineAvailabilityDTO
  ) throws URISyntaxException {
    log.debug(
      "REST request to update BranchVaccineAvailability : {}",
      branchVaccineAvailabilityDTO
    );
    if (branchVaccineAvailabilityDTO.getId() == null) {
      return ResponseEntity
        .badRequest()
        .header("Failure", "The branchVaccineAvailability id cannot be null")
        .body(null);
    }
    BranchVaccineAvailabilityDTO result = branchVaccineAvailabilityService.save(
      branchVaccineAvailabilityDTO
    );
    return ResponseEntity
      .ok()
      .headers(
        HeaderUtil.createEntityUpdateAlert(
          applicationName,
          false,
          ENTITY_NAME,
          branchVaccineAvailabilityDTO.getId().toString()
        )
      )
      .body(result);
  }

  /**
   * {@code GET  /branch-vaccine-availabilities} : get all the branchVaccineAvailabilities.
   *
   * @param pageable the pagination information.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of branchVaccineAvailabilities in body.
   */
  @GetMapping("/branch-vaccine-availabilities")
  public ResponseEntity<List<BranchVaccineAvailabilityDTO>> getAllBranchVaccineAvailabilities(
    Pageable pageable
  ) {
    log.debug("REST request to get a page of BranchVaccineAvailabilities");
    Page<BranchVaccineAvailabilityDTO> page = branchVaccineAvailabilityService.findAll(
      pageable
    );
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
      ServletUriComponentsBuilder.fromCurrentRequest(),
      page
    );
    return ResponseEntity.ok().headers(headers).body(page.getContent());
  }

  /**
   * {@code GET  /branch-vaccine-availabilities/:id} : get the "id" branchVaccineAvailability.
   *
   * @param id the id of the branchVaccineAvailabilityDTO to retrieve.
   * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the branchVaccineAvailabilityDTO, or with status {@code 404 (Not Found)}.
   */
  @GetMapping("/branch-vaccine-availabilities/{id}")
  public ResponseEntity<BranchVaccineAvailabilityDTO> getBranchVaccineAvailability(
    @PathVariable Long id
  ) {
    log.debug("REST request to get BranchVaccineAvailability : {}", id);
    Optional<BranchVaccineAvailabilityDTO> branchVaccineAvailabilityDTO = branchVaccineAvailabilityService.findOne(
      id
    );
    return ResponseUtil.wrapOrNotFound(branchVaccineAvailabilityDTO);
  }

  /**
   * {@code DELETE  /branch-vaccine-availabilities/:id} : delete the "id" branchVaccineAvailability.
   *
   * @param id the id of the branchVaccineAvailabilityDTO to delete.
   * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
   */
  @DeleteMapping("/branch-vaccine-availabilities/{id}")
  public ResponseEntity<Void> deleteBranchVaccineAvailability(
    @PathVariable Long id
  ) {
    log.debug("REST request to delete BranchVaccineAvailability : {}", id);
    branchVaccineAvailabilityService.delete(id);
    return ResponseEntity
      .noContent()
      .headers(
        HeaderUtil.createEntityDeletionAlert(
          applicationName,
          false,
          ENTITY_NAME,
          id.toString()
        )
      )
      .build();
  }
}
