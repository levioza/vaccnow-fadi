/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.errors;

/**
 * The Error constants.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public final class ErrorConstants {

  /**
   * The constant ERR_VALIDATION.
   */
  public static final String ERR_VALIDATION = "error.validation";

  /**
   * The constant ERR_CONSTRAINT_VIOLATION.
   */
  public static final String ERR_CONSTRAINT_VIOLATION =
    "error.constraintViolation";

  /**
   * The constant ERR_CONVERSION_FAILED.
   */
  public static final String ERR_CONVERSION_FAILED = "error.conversionFailed";

  /**
   * The constant ERR_INVALID_DATE.
   */
  public static final String ERR_INVALID_DATE = "error.invalidDate";

  /**
   * The constant ERR_METHOD_NOT_SUPPORTED.
   */
  public static final String ERR_METHOD_NOT_SUPPORTED =
    "error.methodNotSupported";

  /**
   * The constant ERR_VACCINATION.
   */
  public static final String ERR_PROPERTY_REFERENCE_EXCEPTION_SORT =
    "error.propertyReferenceExceptionSort";

  /**
   * The constant ERR_VACCINATION.
   */
  public static final String ERR_VACCINATION = "error.vacination";

  private ErrorConstants() {}
}
