/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.errors;

/**
 * Custom, parameterized exception, which can be translated on the client side.
 * For example:
 *
 * throw new CustomParameterizedException('myCustomError', 'hello', 'world');
 *
 * Can be translated with:
 * "error.myCustomError" :  "The server says {{params[0]}} to {{params[1]}}"
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public class CustomParameterizedException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final String message;
  private final String[] params;

  /**
   * Instantiates a new Custom parameterized exception.
   *
   * @param message the message
   * @param params  the params
   */
  public CustomParameterizedException(String message, String... params) {
    super(message);
    this.message = message;
    this.params = params;
  }

  /**
   * Gets error dto.
   *
   * @return the error dto
   */
  public ParameterizedErrorDTO getErrorDTO() {
    return new ParameterizedErrorDTO(message, params);
  }
}
