/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.errors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO for transferring error message with a list of field errors.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
public class ErrorDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private final String message;
  private final String description;

  private List<FieldErrorDTO> fieldErrors;

  /**
   * Instantiates a new Error dto.
   *
   * @param message the message
   */
  ErrorDTO(String message) {
    this(message, null);
  }

  /**
   * Instantiates a new Error dto.
   *
   * @param message     the message
   * @param description the description
   */
  ErrorDTO(String message, String description) {
    this.message = message;
    this.description = description;
  }

  /**
   * Instantiates a new Error dto.
   *
   * @param message     the message
   * @param description the description
   * @param fieldErrors the field errors
   */
  ErrorDTO(
    String message,
    String description,
    List<FieldErrorDTO> fieldErrors
  ) {
    this.message = message;
    this.description = description;
    this.fieldErrors = fieldErrors;
  }

  /**
   * Add.
   *
   * @param objectName the object name
   * @param field      the field
   * @param message    the message
   */
  public void add(String objectName, String field, String message) {
    if (fieldErrors == null) {
      fieldErrors = new ArrayList<>();
    }
    fieldErrors.add(new FieldErrorDTO(objectName, field, message));
  }

  /**
   * Gets message.
   *
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Gets description.
   *
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Gets field errors.
   *
   * @return the field errors
   */
  public List<FieldErrorDTO> getFieldErrors() {
    return fieldErrors;
  }
}
