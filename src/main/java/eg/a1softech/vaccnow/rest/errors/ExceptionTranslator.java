/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.errors;

import eg.a1softech.vaccnow.service.impl.VaccinationException;
import java.util.List;
import java.util.Optional;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@ControllerAdvice
public class ExceptionTranslator {

  /**
   * Process validation error error dto.
   *
   * @param ex the ex
   * @return the error dto
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorDTO processValidationError(MethodArgumentNotValidException ex) {
    BindingResult result = ex.getBindingResult();
    List<FieldError> fieldErrors = result.getFieldErrors();

    return processFieldErrors(fieldErrors);
  }

  /**
   * Process parameterized validation error parameterized error dto.
   *
   * @param ex the ex
   * @return the parameterized error dto
   */
  @ExceptionHandler(CustomParameterizedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ParameterizedErrorDTO processParameterizedValidationError(
    CustomParameterizedException ex
  ) {
    return ex.getErrorDTO();
  }

  private ErrorDTO processFieldErrors(List<FieldError> fieldErrors) {
    ErrorDTO dto = new ErrorDTO(ErrorConstants.ERR_VALIDATION);

    for (FieldError fieldError : fieldErrors) {
      dto.add(
        fieldError.getObjectName(),
        fieldError.getField(),
        fieldError.getCode()
      );
    }

    return dto;
  }

  /**
   * Process constraint violation.
   *
   * @param ex the exception
   * @return the error dto
   */
  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorDTO processConstraintViolationException(
    ConstraintViolationException ex
  ) {
    return new ErrorDTO(
      ErrorConstants.ERR_CONSTRAINT_VIOLATION,
      ex.getMessage()
    );
  }

  /**
   * Process vaccination exception
   *
   * @param ex the exception
   * @return the error dto
   */
  @ExceptionHandler(VaccinationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorDTO processVaccinationException(VaccinationException ex) {
    return new ErrorDTO(ErrorConstants.ERR_VACCINATION, ex.getMessage());
  }

  /**
   * Process method not supported exception error dto.
   *
   * @param exception the exception
   * @return the error dto
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  public ErrorDTO processMethodNotSupportedException(
    HttpRequestMethodNotSupportedException exception
  ) {
    return new ErrorDTO(
      ErrorConstants.ERR_METHOD_NOT_SUPPORTED,
      exception.getMessage()
    );
  }

  /**
   * Process conversion failed exception.
   *
   * @param ex the exception
   * @return the error dto
   */
  @ExceptionHandler(ConversionFailedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorDTO processConversionFailedException(
    ConversionFailedException ex
  ) {
    Optional<String> exceptionMessage = Optional.ofNullable(ex.getMessage());

    // Check if it's an error related to the LocalDate.
    if (
      exceptionMessage.isPresent() &&
      exceptionMessage.get().contains("DateTime")
    ) {
      return new ErrorDTO(
        ErrorConstants.ERR_INVALID_DATE,
        "Please make sure that the datetime is a valid datetime and exists in the real world!"
      );
    }

    // Else, return the generic error.
    return new ErrorDTO(ErrorConstants.ERR_CONVERSION_FAILED, ex.getMessage());
  }

  /**
   * Process property reference exception sort error dto.
   *
   * @param exception the exception
   * @return the error dto
   */
  @ExceptionHandler(PropertyReferenceException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorDTO processPropertyReferenceExceptionSort(
    PropertyReferenceException exception
  ) {
    return new ErrorDTO(
      ErrorConstants.ERR_PROPERTY_REFERENCE_EXCEPTION_SORT,
      "You can't sort using a non-existing property: " +
      exception.getPropertyName()
    );
  }
}
