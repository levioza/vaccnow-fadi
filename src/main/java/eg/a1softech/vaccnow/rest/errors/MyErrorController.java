/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.rest.errors;

import eg.a1softech.vaccnow.config.profile.Constants;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

/**
 * The Error controller.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@RestController
public class MyErrorController
  implements org.springframework.boot.web.servlet.error.ErrorController {

  @java.lang.SuppressWarnings("java:S1075")
  private static final String PATH = "/error";

  private final Environment env;

  private final ErrorAttributes errorAttributes;

  public MyErrorController(Environment env, ErrorAttributes errorAttributes) {
    this.env = env;
    this.errorAttributes = errorAttributes;
  }

  @Override
  public String getErrorPath() {
    return PATH;
  }

  /**
   * Error view error json dto.
   *
   * @param request  the request
   * @param response the response
   * @return the view error json dto
   */
  @GetMapping(value = PATH)
  public ViewErrorJsonDTO error(
    HttpServletRequest request,
    WebRequest webRequest,
    HttpServletResponse response
  ) {
    // Appropriate HTTP response code (e.g. 404 or 500) is automatically set by Spring.
    // Here we just define response body.
    Integer status = response.getStatus();

    Map<String, Object> myErrorAttributes;

    // Enable error trace when the development profile is enabled.
    Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT)) {
      myErrorAttributes = getErrorAttributes(webRequest, true);
    } else {
      myErrorAttributes = getErrorAttributes(webRequest, false);
    }

    String error = (String) myErrorAttributes.get("error");
    String message = (String) myErrorAttributes.get("message");
    String timeStamp = myErrorAttributes.get("timestamp").toString();
    String trace = (String) myErrorAttributes.get("trace");
    String path = (String) myErrorAttributes.get("path");

    return new ViewErrorJsonDTO(status, error, message, timeStamp, trace, path);
  }

  private Map<String, Object> getErrorAttributes(
    WebRequest webRequest,
    boolean includeStackTrace
  ) {
    // Create a new error attribute options set.
    Set<ErrorAttributeOptions.Include> errorAttributeOptionsIncludeSet = new HashSet<>();

    if (includeStackTrace) {
      errorAttributeOptionsIncludeSet.add(
        ErrorAttributeOptions.Include.STACK_TRACE
      );
    }

    ErrorAttributeOptions errorAttributeOptions = ErrorAttributeOptions.of(
      errorAttributeOptionsIncludeSet
    );
    return this.errorAttributes.getErrorAttributes(
        webRequest,
        errorAttributeOptions
      );
  }
}
