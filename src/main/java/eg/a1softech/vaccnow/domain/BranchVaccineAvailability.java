/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * BranchVaccineAvailability entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Entity
@Table(
  name = "branch_vaccine_availability",
  uniqueConstraints = @UniqueConstraint(
    columnNames = { "vaccination_date_time", "branch_id", "vaccine_id" }
  )
)
public class BranchVaccineAvailability implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "vaccination_date_time", nullable = false)
  private ZonedDateTime vaccinationDateTime;

  @NotNull
  @Column(name = "duration", nullable = false)
  private Integer duration;

  @NotNull
  @Column(name = "available_seats", nullable = false)
  private Integer availableSeats;

  @ManyToOne
  @JsonIgnore
  private Branch branch;

  @ManyToOne
  @JsonIgnore
  private Vaccine vaccine;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ZonedDateTime getVaccinationDateTime() {
    return vaccinationDateTime;
  }

  public BranchVaccineAvailability vaccinationDateTime(
    ZonedDateTime vaccinationDateTime
  ) {
    this.vaccinationDateTime = vaccinationDateTime;
    return this;
  }

  public void setVaccinationDateTime(ZonedDateTime vaccinationDateTime) {
    this.vaccinationDateTime = vaccinationDateTime;
  }

  public Integer getDuration() {
    return duration;
  }

  public BranchVaccineAvailability duration(Integer duration) {
    this.duration = duration;
    return this;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public Integer getAvailableSeats() {
    return availableSeats;
  }

  public BranchVaccineAvailability availableSeats(Integer availableSeats) {
    this.availableSeats = availableSeats;
    return this;
  }

  public void setAvailableSeats(Integer availableSeats) {
    this.availableSeats = availableSeats;
  }

  public Branch getBranch() {
    return branch;
  }

  public BranchVaccineAvailability branch(Branch branch) {
    this.branch = branch;
    return this;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Vaccine getVaccine() {
    return vaccine;
  }

  public BranchVaccineAvailability vaccine(Vaccine vaccine) {
    this.vaccine = vaccine;
    return this;
  }

  public void setVaccine(Vaccine vaccine) {
    this.vaccine = vaccine;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof BranchVaccineAvailability)) {
      return false;
    }
    return id != null && id.equals(((BranchVaccineAvailability) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return (
      "BranchVaccineAvailability{" +
      "id=" +
      getId() +
      ", vaccinationDateTime='" +
      getVaccinationDateTime() +
      "'" +
      ", duration=" +
      getDuration() +
      ", availableSeats=" +
      getAvailableSeats() +
      "}"
    );
  }
}
