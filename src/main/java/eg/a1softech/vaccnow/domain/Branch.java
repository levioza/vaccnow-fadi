/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Branch entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Entity
@Table(
  name = "branch",
  uniqueConstraints = @UniqueConstraint(columnNames = { "name" })
)
public class Branch implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "name", nullable = false)
  private String name;

  @OneToOne
  @JoinColumn(unique = true)
  private Location location;

  @OneToMany(mappedBy = "branch")
  private Set<PatientVaccineAppointment> patientVaccineAppointments = new HashSet<>();

  @OneToMany(mappedBy = "branch")
  private Set<BranchVaccineAvailability> patientVaccineAvailabilities = new HashSet<>();

  @ManyToMany
  @JoinTable(
    name = "branch_vaccine",
    joinColumns = @JoinColumn(name = "branch_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(
      name = "vaccine_id",
      referencedColumnName = "id"
    )
  )
  private Set<Vaccine> vaccines = new HashSet<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public Branch name(String name) {
    this.name = name;
    return this;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Location getLocation() {
    return location;
  }

  public Branch location(Location location) {
    this.location = location;
    return this;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Set<PatientVaccineAppointment> getPatientVaccineAppointments() {
    return patientVaccineAppointments;
  }

  public Branch patientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
    return this;
  }

  public Branch addPatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.add(patientVaccineAppointment);
    patientVaccineAppointment.setBranch(this);
    return this;
  }

  public Branch removePatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.remove(patientVaccineAppointment);
    patientVaccineAppointment.setBranch(null);
    return this;
  }

  public void setPatientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
  }

  public Set<BranchVaccineAvailability> getPatientVaccineAvailabilities() {
    return patientVaccineAvailabilities;
  }

  public Branch patientVaccineAvailabilities(
    Set<BranchVaccineAvailability> branchVaccineAvailabilities
  ) {
    this.patientVaccineAvailabilities = branchVaccineAvailabilities;
    return this;
  }

  public Branch addPatientVaccineAvailability(
    BranchVaccineAvailability branchVaccineAvailability
  ) {
    this.patientVaccineAvailabilities.add(branchVaccineAvailability);
    branchVaccineAvailability.setBranch(this);
    return this;
  }

  public Branch removePatientVaccineAvailability(
    BranchVaccineAvailability branchVaccineAvailability
  ) {
    this.patientVaccineAvailabilities.remove(branchVaccineAvailability);
    branchVaccineAvailability.setBranch(null);
    return this;
  }

  public void setPatientVaccineAvailabilities(
    Set<BranchVaccineAvailability> branchVaccineAvailabilities
  ) {
    this.patientVaccineAvailabilities = branchVaccineAvailabilities;
  }

  public Set<Vaccine> getVaccines() {
    return vaccines;
  }

  public Branch vaccines(Set<Vaccine> vaccines) {
    this.vaccines = vaccines;
    return this;
  }

  public Branch addVaccine(Vaccine vaccine) {
    this.vaccines.add(vaccine);
    vaccine.getBranches().add(this);
    return this;
  }

  public Branch removeVaccine(Vaccine vaccine) {
    this.vaccines.remove(vaccine);
    vaccine.getBranches().remove(this);
    return this;
  }

  public void setVaccines(Set<Vaccine> vaccines) {
    this.vaccines = vaccines;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Branch)) {
      return false;
    }
    return id != null && id.equals(((Branch) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return "Branch{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
  }
}
