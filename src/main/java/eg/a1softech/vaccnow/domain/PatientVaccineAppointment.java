/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eg.a1softech.vaccnow.domain.enumeration.PaymentMethod;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * PatientVaccineAppointment entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Entity
@Table(
  name = "patient_vaccine_appointment",
  uniqueConstraints = @UniqueConstraint(
    columnNames = {
      "vaccination_date_time", "branch_id", "patient_id", "vaccine_id",
    }
  )
)
public class PatientVaccineAppointment implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "payment_method", nullable = false)
  private PaymentMethod paymentMethod;

  @NotNull
  @Column(name = "vaccination_date_time", nullable = false)
  private ZonedDateTime vaccinationDateTime;

  @Column(name = "fawry_transaction_id")
  private String fawryTransactionId;

  @Column(name = "credit_card_last_4_digits")
  private String creditCardLast4Digits;

  @Column(name = "amount_paid", precision = 21, scale = 2)
  private BigDecimal amountPaid;

  @Column(name = "confirmed")
  private Boolean confirmed;

  @Column(name = "taken")
  private Boolean taken;

  @ManyToOne
  @JsonIgnore
  private Branch branch;

  @ManyToOne
  @JsonIgnore
  private Patient patient;

  @ManyToOne
  @JsonIgnore
  private Vaccine vaccine;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public PaymentMethod getPaymentMethod() {
    return paymentMethod;
  }

  public PatientVaccineAppointment paymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
    return this;
  }

  public void setPaymentMethod(PaymentMethod paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public ZonedDateTime getVaccinationDateTime() {
    return vaccinationDateTime;
  }

  public PatientVaccineAppointment vaccinationDateTime(
    ZonedDateTime vaccinationDateTime
  ) {
    this.vaccinationDateTime = vaccinationDateTime;
    return this;
  }

  public void setVaccinationDateTime(ZonedDateTime vaccinationDateTime) {
    this.vaccinationDateTime = vaccinationDateTime;
  }

  public String getFawryTransactionId() {
    return fawryTransactionId;
  }

  public PatientVaccineAppointment fawryTransactionId(
    String fawryTransactionId
  ) {
    this.fawryTransactionId = fawryTransactionId;
    return this;
  }

  public void setFawryTransactionId(String fawryTransactionId) {
    this.fawryTransactionId = fawryTransactionId;
  }

  public String getCreditCardLast4Digits() {
    return creditCardLast4Digits;
  }

  public PatientVaccineAppointment creditCardLast4Digits(
    String creditCardLast4Digits
  ) {
    this.creditCardLast4Digits = creditCardLast4Digits;
    return this;
  }

  public void setCreditCardLast4Digits(String creditCardLast4Digits) {
    this.creditCardLast4Digits = creditCardLast4Digits;
  }

  public BigDecimal getAmountPaid() {
    return amountPaid;
  }

  public PatientVaccineAppointment amountPaid(BigDecimal amountPaid) {
    this.amountPaid = amountPaid;
    return this;
  }

  public void setAmountPaid(BigDecimal amountPaid) {
    this.amountPaid = amountPaid;
  }

  public Boolean isConfirmed() {
    return confirmed;
  }

  public PatientVaccineAppointment confirmed(Boolean confirmed) {
    this.confirmed = confirmed;
    return this;
  }

  public void setConfirmed(Boolean confirmed) {
    this.confirmed = confirmed;
  }

  public Boolean isTaken() {
    return taken;
  }

  public PatientVaccineAppointment taken(Boolean taken) {
    this.taken = taken;
    return this;
  }

  public void setTaken(Boolean taken) {
    this.taken = taken;
  }

  public Branch getBranch() {
    return branch;
  }

  public PatientVaccineAppointment branch(Branch branch) {
    this.branch = branch;
    return this;
  }

  public void setBranch(Branch branch) {
    this.branch = branch;
  }

  public Patient getPatient() {
    return patient;
  }

  public PatientVaccineAppointment patient(Patient patient) {
    this.patient = patient;
    return this;
  }

  public void setPatient(Patient patient) {
    this.patient = patient;
  }

  public Vaccine getVaccine() {
    return vaccine;
  }

  public PatientVaccineAppointment vaccine(Vaccine vaccine) {
    this.vaccine = vaccine;
    return this;
  }

  public void setVaccine(Vaccine vaccine) {
    this.vaccine = vaccine;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof PatientVaccineAppointment)) {
      return false;
    }
    return id != null && id.equals(((PatientVaccineAppointment) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return (
      "PatientVaccineAppointment{" +
      "id=" +
      getId() +
      ", paymentMethod='" +
      getPaymentMethod() +
      "'" +
      ", vaccinationDateTime='" +
      getVaccinationDateTime() +
      "'" +
      ", fawryTransactionId='" +
      getFawryTransactionId() +
      "'" +
      ", creditCardLast4Digits='" +
      getCreditCardLast4Digits() +
      "'" +
      ", amountPaid=" +
      getAmountPaid() +
      ", confirmed='" +
      isConfirmed() +
      "'" +
      ", taken='" +
      isTaken() +
      "'" +
      "}"
    );
  }
}
