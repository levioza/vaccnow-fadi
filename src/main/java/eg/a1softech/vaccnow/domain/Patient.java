/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Patient entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Entity
@Table(
  name = "patient",
  uniqueConstraints = @UniqueConstraint(
    columnNames = { "email", "phone_number" }
  )
)
public class Patient implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "first_name", nullable = false)
  private String firstName;

  @NotNull
  @Column(name = "last_name", nullable = false)
  private String lastName;

  @NotNull
  @Column(name = "email", nullable = false)
  private String email;

  @NotNull
  @Column(name = "phone_number", nullable = false)
  private String phoneNumber;

  @OneToMany(mappedBy = "patient")
  private Set<PatientVaccineAppointment> patientVaccineAppointments = new HashSet<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public Patient firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public Patient lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public Patient email(String email) {
    this.email = email;
    return this;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public Patient phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Set<PatientVaccineAppointment> getPatientVaccineAppointments() {
    return patientVaccineAppointments;
  }

  public Patient patientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
    return this;
  }

  public Patient addPatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.add(patientVaccineAppointment);
    patientVaccineAppointment.setPatient(this);
    return this;
  }

  public Patient removePatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.remove(patientVaccineAppointment);
    patientVaccineAppointment.setPatient(null);
    return this;
  }

  public void setPatientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Patient)) {
      return false;
    }
    return id != null && id.equals(((Patient) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return (
      "Patient{" +
      "id=" +
      getId() +
      ", firstName='" +
      getFirstName() +
      "'" +
      ", lastName='" +
      getLastName() +
      "'" +
      ", email='" +
      getEmail() +
      "'" +
      ", phoneNumber='" +
      getPhoneNumber() +
      "'" +
      "}"
    );
  }
}
