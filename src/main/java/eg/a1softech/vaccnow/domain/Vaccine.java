/*
 * Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package eg.a1softech.vaccnow.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Vaccine entity.
 *
 * @author Fadi William Ghali Abdelmessih.
 */
@Entity
@Table(
  name = "vaccine",
  uniqueConstraints = @UniqueConstraint(columnNames = { "name" })
)
public class Vaccine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(
    strategy = GenerationType.SEQUENCE,
    generator = "sequenceGenerator"
  )
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "name", nullable = false)
  private String name;

  @OneToMany(mappedBy = "vaccine")
  private Set<PatientVaccineAppointment> patientVaccineAppointments = new HashSet<>();

  @OneToMany(mappedBy = "vaccine")
  private Set<BranchVaccineAvailability> patientVaccineAvailabilities = new HashSet<>();

  @ManyToMany(mappedBy = "vaccines")
  @JsonIgnore
  private Set<Branch> branches = new HashSet<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public Vaccine name(String name) {
    this.name = name;
    return this;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<PatientVaccineAppointment> getPatientVaccineAppointments() {
    return patientVaccineAppointments;
  }

  public Vaccine patientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
    return this;
  }

  public Vaccine addPatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.add(patientVaccineAppointment);
    patientVaccineAppointment.setVaccine(this);
    return this;
  }

  public Vaccine removePatientVaccineAppointment(
    PatientVaccineAppointment patientVaccineAppointment
  ) {
    this.patientVaccineAppointments.remove(patientVaccineAppointment);
    patientVaccineAppointment.setVaccine(null);
    return this;
  }

  public void setPatientVaccineAppointments(
    Set<PatientVaccineAppointment> patientVaccineAppointments
  ) {
    this.patientVaccineAppointments = patientVaccineAppointments;
  }

  public Set<BranchVaccineAvailability> getPatientVaccineAvailabilities() {
    return patientVaccineAvailabilities;
  }

  public Vaccine patientVaccineAvailabilities(
    Set<BranchVaccineAvailability> branchVaccineAvailabilities
  ) {
    this.patientVaccineAvailabilities = branchVaccineAvailabilities;
    return this;
  }

  public Vaccine addPatientVaccineAvailability(
    BranchVaccineAvailability branchVaccineAvailability
  ) {
    this.patientVaccineAvailabilities.add(branchVaccineAvailability);
    branchVaccineAvailability.setVaccine(this);
    return this;
  }

  public Vaccine removePatientVaccineAvailability(
    BranchVaccineAvailability branchVaccineAvailability
  ) {
    this.patientVaccineAvailabilities.remove(branchVaccineAvailability);
    branchVaccineAvailability.setVaccine(null);
    return this;
  }

  public void setPatientVaccineAvailabilities(
    Set<BranchVaccineAvailability> branchVaccineAvailabilities
  ) {
    this.patientVaccineAvailabilities = branchVaccineAvailabilities;
  }

  public Set<Branch> getBranches() {
    return branches;
  }

  public Vaccine branches(Set<Branch> branches) {
    this.branches = branches;
    return this;
  }

  public Vaccine addBranch(Branch branch) {
    this.branches.add(branch);
    branch.getVaccines().add(this);
    return this;
  }

  public Vaccine removeBranch(Branch branch) {
    this.branches.remove(branch);
    branch.getVaccines().remove(this);
    return this;
  }

  public void setBranches(Set<Branch> branches) {
    this.branches = branches;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Vaccine)) {
      return false;
    }
    return id != null && id.equals(((Vaccine) o).id);
  }

  @Override
  public int hashCode() {
    return 31;
  }

  @Override
  public String toString() {
    return "Vaccine{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
  }
}
