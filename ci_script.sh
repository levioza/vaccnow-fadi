#!/usr/bin/env bash

# Clean
mvn clean

# Compile
mvn compile

# mvn verify and execute sonar.
mvn verify sonar:sonar \
    -Dsonar.host.url=${SONAR_URL_HOST} \
    -Dsonar.organization=${SONAR_ORGANIZATION} \
    -Dsonar.projectKey=${SONAR_PROJECT_KEY} \
    -Dsonar.login=${SONAR_LOGIN}
