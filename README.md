# Vaccnow

[![Build Status](https://gitlab.com/levioza/vaccnow-fadi/badges/master/pipeline.svg)](https://gitlab.com/levioza/vaccnow-fadi/-/pipelines)
[![Code Coverage](https://img.shields.io/sonar/https/sonarcloud.io/fadiabdelmessih_vaccnow/coverage.svg)](https://sonarcloud.io/dashboard?id=fadiabdelmessih_vaccnow)

This is a sample project for the fictional company VaccNow. VaccNow is a healthcare organization managing the process of distributing the Covid-19 vaccine to the public.

## The project uses the following technologies:

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Postgres](https://www.postgresql.org)
- [H2](https://www.h2database.com)
- [Liquibase](https://www.liquibase.org)

## Some screenshots of the project

<div align="center">
    <img src="./images/1.png" alt="Vaccine Appointment Confirmation" width="200px" />
    <img src="./images/2.png" alt="Vaccination Certificate" width="200px" /> 
    <img src="./images/swagger.png" alt="Swagger API" width="200px" /> 
</div>

## How to run the project

### Development

During development, the application bootstraps itself. The liquibase changelog runs automatically. So, you could test right away.

To run the application in development, please follow the following steps.

1. Make sure you have mvn installed on your platform by following `https://maven.apache.org/install.html`
2. Make sure you have docker & docker-compose installed on your platform using their official [website](https://www.docker.com/products/docker-desktop). I use Docker during development as I'm using the [MailHog](https://github.com/mailhog/MailHog) email server for testing.
3. Run `docker-compose -f docker-compose.dev.yml up` in a cli/terminal.
4. Run `mvn spring-boot:run -Dspring.profiles.active=dev` in a separate terminal.
5. Now, you can test the apis by visiting `http://localhost:8082/swagger-ui`.

#### Notes

* Make sure so the `JAVA_HOME` environmental variable points to JDK11 or greater.
* If you found any issues or problems while setting up the Java JDK11 or greater. Please contact me at `fadi.william.ghali@gmail.com`.

### Production

1. Make sure you have mvn installed on your platform by following `https://maven.apache.org/install.html`
2. Configure `application-prod.yml` to point to your production grade Postgres SQL (Make sure to use the right db_name, username & password).
3. Configure `application-prod.yml` with the appropriate smtp server information. I highly recommend [SendGrid](https://sendgrid.com) in production.
3. Run `mvn spring-boot:run`

#### Notes

* It's not recommended to use secrets like the database connection information or the smtp server information in plain in the configuration files. So, I recommend that you set the values in `application-prod.yml` to environmental variables while configuring an appropriate pipeline to deploy the application to a server. Containers/Kubernetes might be an option to deploy your application; in such case, you may set the environment variables as Kubernetes secrets. 
* For further insights on how to dockerize spring boot, perform ci/cd with kubernetes. Please contact me at `fadi.william.ghali@gmail.com`.

### License

```
Copyright (c) 2021 Fadi William Ghali Abdelmessih<fadi.william.ghali@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be included in all copies
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```
