<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.1</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>eg.a1softech</groupId>
	<artifactId>vaccnow</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>vaccnow</name>
	<description>This is a sample project for the fictional company VaccNow. VaccNow is a healthcare organization managing the process of distributing the Covid-19 vaccine to the public.</description>

	<properties>
		<!-- Java Version -->
		<java.version>11</java.version>
		<!-- Validation API Version -->
		<validation-api.version>2.0.1.Final</validation-api.version>
		<!-- Sonar - Code Analysis -->
		<sonar-maven-plugin.version>3.7.0.1746</sonar-maven-plugin.version>
		<!-- Jacoco - Code Coverage -->
		<jacoco-maven-plugin.version>0.8.6</jacoco-maven-plugin.version>
		<!-- ArchUnit JUNIT 5 -->
		<archunit-junit5.version>0.14.1</archunit-junit5.version>
		<!-- Maven Properties -->
		<properties-maven-plugin.version>1.0.0</properties-maven-plugin.version>
		<!-- Liquibase Related properties -->
		<liquibase.version>4.2.2</liquibase.version>
		<liquibase-hibernate5.version>4.2.2</liquibase-hibernate5.version>
		<javassist.version>3.24.0-GA</javassist.version>
		<spring.boot.verion>2.4.1</spring.boot.verion>
	</properties>

	<dependencies>
		<!-- Spring data JPA -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<!-- Migration management for SQL Databases -->
		<dependency>
			<groupId>org.liquibase</groupId>
			<artifactId>liquibase-core</artifactId>
			<version>4.2.2</version>
		</dependency>

		<!-- Fast, simple, reliable. HikariCP is a "zero-overhead" production ready JDBC connection pool -->
		<dependency>
			<groupId>com.zaxxer</groupId>
			<artifactId>HikariCP</artifactId>
		</dependency>

		<!-- In memory database -->
		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
		</dependency>

		<!-- Postgres SQL For Production -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
		</dependency>

		<!-- Add MapStruct to manage the mapping beteen the entity and its dto representation -->
		<dependency>
			<groupId>org.mapstruct</groupId>
			<artifactId>mapstruct</artifactId>
			<version>1.3.1.Final</version>
		</dependency>
		<dependency>
			<groupId>org.mapstruct</groupId>
			<artifactId>mapstruct-processor</artifactId>
			<version>1.3.1.Final</version>
			<scope>provided</scope>
		</dependency>

		<!-- Javax Validation & Hibernate Validator -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>${validation-api.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate.validator</groupId>
			<artifactId>hibernate-validator</artifactId>
		</dependency>

		<!-- Email support Spring Boot -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>

		<!-- Template Engine for Emails -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>

		<!-- Aspect Logging -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>

		<!-- Logging -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-logging</artifactId>
		</dependency>

		<!-- Spring Boot Starter Web -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<!-- Currently, the new tomcat doesn't support brackets in the url -->
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-tomcat</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- use undertow instead of tomcat -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-undertow</artifactId>
		</dependency>

		<!-- Swagger Documentation Dependencies -->
		<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-ui</artifactId>
			<version>1.5.2</version>
		</dependency>
		<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-data-rest</artifactId>
			<version>1.5.2</version>
		</dependency>

		<!-- Spring Boot Dev Tools -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>

		<!-- Spring Boot Test Support -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- JUnit5 - Arch Unit -->
		<dependency>
			<groupId>com.tngtech.archunit</groupId>
			<artifactId>archunit-junit5-api</artifactId>
			<version>${archunit-junit5.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.liquibase</groupId>
				<artifactId>liquibase-maven-plugin</artifactId>
				<version>4.2.2</version>
				<dependencies>
					<dependency>
						<groupId>org.liquibase</groupId>
						<artifactId>liquibase-core</artifactId>
						<version>${liquibase.version}</version>
					</dependency>
					<dependency>
						<groupId>org.liquibase.ext</groupId>
						<artifactId>liquibase-hibernate5</artifactId>
						<version>${liquibase-hibernate5.version}</version>
					</dependency>
					<dependency>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-starter-data-jpa</artifactId>
						<version>${spring.boot.verion}</version>
					</dependency>
					<dependency>
						<groupId>javax.validation</groupId>
						<artifactId>validation-api</artifactId>
						<version>${validation-api.version}</version>
					</dependency>
					<dependency>
						<groupId>org.javassist</groupId>
						<artifactId>javassist</artifactId>
						<version>${javassist.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<changeLogFile>${project.basedir}/src/main/resources/config/liquibase/master.xml</changeLogFile>
					<diffChangeLogFile>${project.basedir}/src/main/resources/config/liquibase/changelog/${maven.build.timestamp}_changelog.xml</diffChangeLogFile>
					<driver>org.postgresql.Driver</driver>
					<url>jdbc:postgresql://localhost:5432/vaccnow_db?user=postgres&amp;password=pass123</url>
					<referenceUrl>hibernate:spring:eg.a1softech.vaccnow.domain?dialect=org.hibernate.dialect.PostgreSQL82Dialect&amp;hibernate.physical_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy&amp;hibernate.implicit_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy</referenceUrl>
					<verbose>true</verbose>
					<logging>debug</logging>
					<contexts>!test</contexts>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<!-- The coverage profile -->
		<profile>
			<id>code-analysis</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.sonarsource.scanner.maven</groupId>
						<artifactId>sonar-maven-plugin</artifactId>
						<version>${sonar-maven-plugin.version}</version>
					</plugin>
					<plugin>
						<groupId>org.jacoco</groupId>
						<artifactId>jacoco-maven-plugin</artifactId>
						<version>${jacoco-maven-plugin.version}</version>
						<executions>
							<execution>
								<id>default-prepare-agent</id>
								<goals>
									<goal>prepare-agent</goal>
								</goals>
							</execution>
							<execution>
								<id>default-report</id>
								<phase>prepare-package</phase>
								<goals>
									<goal>report</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>properties-maven-plugin</artifactId>
					</plugin>
				</plugins>
				<pluginManagement>
					<plugins>
						<plugin>
							<groupId>org.codehaus.mojo</groupId>
							<artifactId>properties-maven-plugin</artifactId>
							<version>${properties-maven-plugin.version}</version>
							<executions>
								<execution>
									<phase>initialize</phase>
									<goals>
										<goal>read-project-properties</goal>
									</goals>
									<configuration>
										<files>
											<file>sonar-project.properties</file>
										</files>
									</configuration>
								</execution>
							</executions>
						</plugin>
					</plugins>
				</pluginManagement>
			</build>
		</profile>
		<!-- The dev profile -->
		<profile>
			<id>dev</id>
			<properties>
				<!-- Default Spring profile -->
				<spring.profiles.active>dev</spring.profiles.active>
			</properties>
		</profile>
		<!-- The prod profile -->
		<profile>
			<id>prod</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<!-- Default Spring profile -->
				<spring.profiles.active>prod</spring.profiles.active>
			</properties>
		</profile>
	</profiles>

</project>
